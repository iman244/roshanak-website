/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    remotePatterns: [
      {
        protocol: "http",
        hostname: "188.121.101.112",
        port: "8081",
      },
      {
        protocol: "https",
        hostname: "188.121.101.112",
        port: "8081",
      },
    ],
  },
};

module.exports = nextConfig;
