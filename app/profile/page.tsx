"use client";

import { Button, Flex, useBreakpointValue } from "@chakra-ui/react";
import { useState } from "react";
import PurchasedPackagesContainer from "../Containers/PurchasedPackagesContainer";
import { AnimatePresence, motion } from "framer-motion";
import { SelectComponenet } from "../components/SelectComponent";
import PurchasedPackagesPresenter from "../Presenters/PurchasedPackagesPresenter";
import EditProfilePresenter from "../Presenters/EditProfilePresenter";
import { SubscriptionManagementPresenter } from "../Presenters/SubscriptionManagementPresenter";
import ProfileContainer from "../Containers/ProfileContainer";
import { LogoutButton } from "../components/LogoutButton";

type ProfileURLs = string;

export default function Page({ children }: { children: React.ReactNode }) {
  const [section, setSection] = useState({
    value: "purchased-packages",
    label: "دوره های خریداری شده",
  });

  const show = useBreakpointValue({
    base: true,
    md: false,
  });

  function container(section: ProfileURLs) {
    switch (section) {
      case "purchased-packages":
        return (
          <PurchasedPackagesContainer>
            <PurchasedPackagesPresenter />
          </PurchasedPackagesContainer>
        );
      case "subscription-management":
        return <SubscriptionManagementPresenter />;
      case "edit-profile":
        return (
          <ProfileContainer>
            <EditProfilePresenter />
          </ProfileContainer>
        );
      default:
        <PurchasedPackagesContainer>
          <PurchasedPackagesPresenter />
        </PurchasedPackagesContainer>;
    }
  }

  return (
    <Flex
      gap={"32px"}
      dir={"rtl"}
      padding={"40px"}
      flexDir={{ base: "column", md: "row" }}
    >
      <nav>
        <Flex
          flexDir={"column"}
          padding={"2px"}
          w={{ base: "100%", md: "fit-content" }}
          bgGradient={
            show
              ? "linear-gradient(0deg, #B549F6 0%, #B549F6 45.29%, #B549F6 99.95%);"
              : "linear-gradient(0deg, #21C6E2 0%, #6B88EB 45.29%, #B549F6 99.95%);"
          }
          gap={"2px"}
          mb={"24px"}
          className="profileNavbar"
        >
          <SelectComponenet
            value={section}
            isClearable={false}
            selectedOption={section}
            setSelectedOption={setSection}
            options={[
              { label: "دوره های خریداری شده", value: "purchased-packages" },
              { label: "مدیریت اشتراک", value: "subscription-management" },
              { label: "تنظیمات پروفایل", value: "edit-profile" },
            ]}
          />

          {[
            { label: "دوره های خریداری شده", value: "purchased-packages" },
            { label: "مدیریت اشتراک", value: "subscription-management" },
            { label: "تنظیمات پروفایل", value: "edit-profile" },
          ].map((item) => (
            <Button
              display={{ base: "none", md: "block" }}
              borderRadius={"0"}
              bgColor={
                section.value == item.value
                  ? "transparent"
                  : "rgba(52, 52, 52, 1)"
              }
              size={"lg"}
              color={"white"}
              fontWeight={400}
              fontSize={"sm"}
              _hover={{ bgColor: "transparent" }}
              onClick={() => setSection(item)}
              textAlign={"start"}
            >
              {item.label}
            </Button>
          ))}
        </Flex>
        <LogoutButton />
      </nav>
      <Flex flexDir={"column"} alignItems={"center"} w={"100%"}>
        <AnimatePresence
          initial={false}
          mode="wait"
          onExitComplete={() => window.scrollTo(0, 0)}
        >
          <motion.div
            key={section.value}
            initial="hidden" // Set the initial state to variants.hidden
            animate="enter" // Animated state to variants.enter
            exit="exit" // Exit state (used later) to variants.exit
            transition={{
              type: "easeOut",
              delayChildren: 0.3,
              staggerChildren: 0.2,
            }} //
            variants={{
              hidden: {
                opacity: 0,
                // x: "-100%",
              },
              enter: {
                opacity: 1,
                // x: 0,
              },
              exit: {
                opacity: 0,
              },
            }}
            className="base-page-size"
            style={{
              height: "100%",
              width: "100%",
              boxSizing: "border-box",
            }}
          >
            {container(section.value)}
          </motion.div>
        </AnimatePresence>
      </Flex>
    </Flex>
  );
}
