import { QueryStatus } from "react-query";

interface polished_package {
  category: string;
  discounted_price: number;
  is_for_premium: boolean;
  is_owned: boolean;
  level: any;
  name: string;
  covers: string[];
  price: number;
  slug: string;
}

export interface polished_package_noVideo extends polished_package {
  lessons: simple_lesson[];
}

export interface polished_package_withVideo extends polished_package {
  lessons: full_lesson[];
}

export interface query<T> {
  data: T;
  status: QueryStatus;
}

export interface simple_lesson {
  title: string;
  description: string;
  voice: string;
}

export interface full_lesson {
  title: string;
  description: string;
  voice: string;
  ordering: number;
  is_teaser: boolean;
  video: string;
  videos: videoInfo[];
  pdf: string;
  cover: string;
  midi: string;
  student_videos: [];
}

export interface videoInfo {
  title: string;
  ordering: number;
  video: string;
}

export interface extracted_lesson_for_player {
  title: string;
  cover: string;
  pdf: string;
  midi: string;
  videos: videoInfo[];
  nextLessonURL: string;
  package: string;
}

export interface abstract_lesson {
  title: string;
  ordering: number;
  cover: string;
  package: string;
}
