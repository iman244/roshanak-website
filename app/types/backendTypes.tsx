export interface backend_packages {
  packages_list: [backend_package_noVideo[], number];
}

export interface backend401 {
  message: "Authentication credentials were not provided.";
}

interface backend_basicData_package {
  category: string;
  discounted_price: number;
  is_for_premium: boolean;
  is_owned: boolean;
  level: any;
  name: string;
  package_covers: coverObject[];
  price: number;
  slug: string;
}

export interface backend_package_noVideo extends backend_basicData_package {
  lessons: backend_simple_lesson[];
}

export interface backend_package_withVideo extends backend_basicData_package {
  lessons: backend_full_lesson[];
}

export interface coverObject {
  cover: fileObject;
}

export interface backend_profile {
  first_name: string;
  last_name: string;
  phone_number: string;
  email: string;
  birthdate: null;
  gender: null;
}

export interface backend_purchased_packages {
  owned_packages: backend_package_withVideo[];
}

interface backend_simple_lesson {
  title: string;
  description: string;
  voice: fileObject;
}

interface backend_full_lesson {
  title: string;
  description: string;
  voice: fileObject;
  ordering: number;
  is_teaser: boolean;
  midi: fileObject;
  video: videoObject;
  videos: backendVideoInfo[];
  pdf: fileObject;
  cover: fileObject;
  student_videos: [];
}

interface backendVideoInfo {
  has_midi: boolean;
  lesson: string;
  title: string;
  video: fileObject;
}

interface videoObject {
  hls_link: string;
  uploaded_video: fileObject;
}

export interface LoginSuccessData {
  access_token: string;
  refresh_token: string;
}

export interface LoginErrorData {
  message: string;
}

export interface LoginDTO {
  email: string;
  password: string;
}

export interface fileObject {
  file: string;
}
