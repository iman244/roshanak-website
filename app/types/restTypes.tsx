import { AxiosResponse } from "axios";

export type GETsuccessThenFunctions = ((data: any) => any) | undefined;
export type GETunknownThenFunctions =
  | ((data: any, error: unknown) => any)
  | undefined;
export type GETerrorThenFunctions = ((err: unknown) => any) | undefined;

export type POSTsuccessThenFunctions =
  | ((
      data: any,
      d: AxiosResponse<any, any>,
      variables: void,
      context: unknown
    ) => void | Promise<unknown>)
  | undefined;
export type POSTunknownThenFunctions =
  | ((
      data: AxiosResponse<any, any> | undefined,
      error: unknown,
      variables: void,
      context: unknown
    ) => void | Promise<unknown>)
  | undefined;
export type POSTerrorThenFunctions =
  | ((
      error: unknown,
      variables: void,
      context: unknown
    ) => void | Promise<unknown>)
  | undefined;
