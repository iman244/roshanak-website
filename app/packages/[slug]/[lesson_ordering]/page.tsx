"use client";
import React from "react";
import { LessonContainer } from "@/app/Containers/LessonContainer";
import LessonPlayerPresenter from "@/app/Presenters/LessonPlayerPresenter";
import { PackageContainer } from "@/app/Containers/PackageContainer";
import { PackagesListSimplePresenter } from "@/app/Presenters/PackagesListSimplePresenter";
import { LessonsListPresenter } from "@/app/Presenters/LessonsListPresenter";
import { LessonsContainer } from "@/app/Containers/LessonsContainer";

// there is no api to get a single package fully. so we don't use useContainer

export default function LessonPlayer(props: any) {
  const { slug, lesson_ordering } = props.params;

  return (
    <>
      <LessonContainer slug={slug} lesson_ordering={lesson_ordering}>
        <LessonPlayerPresenter />
      </LessonContainer>
      <LessonsContainer
        slug={slug}
        exception={true}
        lesson_ordering={lesson_ordering}
      >
        <LessonsListPresenter />
      </LessonsContainer>
    </>
  );
}
