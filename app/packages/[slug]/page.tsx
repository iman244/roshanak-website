"use client";
import { polishPackageNoVideo } from "@/app/Functions/polishBackendData";
import { backend_package_noVideo } from "@/app/types/backendTypes";
import { polished_package_noVideo } from "@/app/types/frontTypes";
import React, { useEffect, useState } from "react";
import Suspense from "@/app/components/Suspense";
import { useRouter } from "next/navigation";
import { useContainer } from "@/app/hooks/useContainer";
import PurchasedPackagePresenter from "../../Presenters/PurchasedPackagePresenter";
import PackagePresenter from "@/app/Presenters/PackagePresenter";
import { PackageContainer } from "@/app/Containers/PackageContainer";

export default function Package(props: any) {
  const { slug } = props.params;
  const router = useRouter();

  const [purchased, setPurchased] = useState<boolean>();
  const [not_found, setNotFound] = useState(true);

  const packageSlug = useContainer<polished_package_noVideo>(
    `packages/${slug}`,
    {
      keys: [slug],
      defaultValue: undefined,
      case200: (data: backend_package_noVideo) => {
        setNotFound(false);
        return polishPackageNoVideo(data);
      },

      case404(data) {
        router.push("/not-found");
      },
    }
  );

  useEffect(() => {
    if (packageSlug.data) {
      setPurchased(packageSlug.data.is_owned);
    }
  }, [packageSlug]);

  return (
    <Suspense isLoading={not_found || purchased == undefined}>
      <PackageContainer slug={slug}>
        {purchased ? <PurchasedPackagePresenter /> : <PackagePresenter />}
      </PackageContainer>
    </Suspense>
  );
}
