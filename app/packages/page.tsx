"use client";

import React, { useState } from "react";
import PackagesPresenter from "../Presenters/PackagesPresenter";
import { Button, Flex, Heading } from "@chakra-ui/react";
import { PackagesContainer } from "../Containers/PackagesContainer";

export default function Packages(props: any) {
  return (
    <main style={{ direction: "rtl" }}>
      <PackagesContainer>
        <PackagesPresenter />
      </PackagesContainer>
    </main>
  );
}
