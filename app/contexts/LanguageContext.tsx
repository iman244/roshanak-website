"use client";

import { createContext, useState } from "react";

export const LanguageContext = createContext({
  language: "fa",
});

export function LanguageContextWrapper({ children }: any) {
  const [language, setLanguage] = useState("fa");

  var Q;
  var url = "/accounts/refresh/";

  return (
    <LanguageContext.Provider value={{ language, setLanguage }}>
      {children}
    </LanguageContext.Provider>
  );
}
