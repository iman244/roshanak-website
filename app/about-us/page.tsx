"use client";

import React from "react";
import { FlexMain } from "../components/FlexMain";
import { InfoBox } from "../components/TextComponenets";
import { Box, Flex, Heading, Icon, Text } from "@chakra-ui/react";
import { InstallLink } from "../components/InstallLink";
import { FaTelegram } from "react-icons/fa";
import { GrayBox } from "../components/GrayBox";
import { contactUsList } from "../page";
import { BiLogoInstagramAlt } from "react-icons/bi";
import { FiAtSign } from "react-icons/fi";
import { IoLogoWhatsapp } from "react-icons/io";

export default function AboutUs(props: any) {
  return (
    <FlexMain>
      <InfoBox heading="روشنک پیانو" headingAlign="center">
        <Text fontSize={"0.75em"}>
          توضیحات درباره روشنک پیانو توضیحات درباره روشنک پیانو توضیحات درباره
          روشنک پیانو توضیحات درباره روشنک پیانو توضیحات درباره روشنک پیانو
          توضیحات درباره روشنک پیانو توضیحات درباره روشنک پیانو توضیحات درباره
          روشنک پیانو توضیحات درباره روشنک پیانو توضیحات درباره روشنک پیانو
          توضیحات درباره روشنک پیانو توضیحات درباره روشنک پیانو توضیحات درباره
          روشنک پیانو توضیحات درباره روشنک پیانو توضیحات درباره روشنک پیانو
          توضیحات درباره روشنک پیانو توضیحات درباره روشنک پیانو توضیحات درباره
          روشنک پیانو توضیحات درباره روشنک پیانو توضیحات درباره روشنک پیانو
          توضیحات درباره روشنک پیانو توضیحات درباره روشنک پیانو توضیحات درباره
          روشنک پیانو توضیحات درباره روشنک پیانو توضیحات درباره روشنک پیانو
          توضیحات درباره روشنک پیانو توضیحات درباره روشنک پیانو توضیحات درباره
          روشنک پیانو توضیحات درباره روشنک پیانو توضیحات درباره روشنک پیانو
          توضیحات درباره روشنک پیانو توضیحات درباره روشنک پیانو ت
        </Text>
      </InfoBox>
      <Flex
        flexDir={"row"}
        flexWrap={"wrap"}
        alignItems={"center"}
        justifyContent={"center"}
        gap={"16px"}
        mt={"24px"}
      >
        {contactUsList.map((item) => (
          <InstallLink src={item.src} href={item.href} text={item.text} />
        ))}
      </Flex>
      <Box
        mt={"24px"}
        bgColor={"gray"}
        w={"100%"}
        h={"calc((100vw - 200px) / 16 * 9)"}
      ></Box>

      <Flex justifyContent={"space-between"} w={"100%"} mt={"24px"}>
        <Flex flexWrap={"wrap"} gap={"16px"} maxW={"450px"}>
          {AboutUsContactList.map((v, index) => {
            var order = index;
            return (
              <ContactInfo
                order={order}
                content={v.content}
                icon={v.icon}
                heading={v.heading}
              />
            );
          })}
        </Flex>
        <Flex
          padding={"16px"}
          bgColor={"rgba(217, 217, 217, 0.1)"}
          w={{ base: "100%", md: "200px" }}
          height={"fit-content"}
          h={""}
          minH={"100%"}
          flexDir={"column"}
          gap={"16px"}
        >
          <Heading fontWeight={300} fontSize={"0.875em"} color={"gray"}>
            نشانی:
          </Heading>
          <Text fontSize={"0.75em"} color={"gray"}>
            content
          </Text>
        </Flex>
      </Flex>
    </FlexMain>
  );
}

const ContactInfo = ({
  order,
  content,
  heading,
  icon,
}: {
  order: number;
  content: string;
  heading: string;
  icon: any;
}) => {
  return (
    <Flex
      w={"200px"}
      h={"120px"}
      bgColor={"rgba( 100 , 100 , 100,0.5)"}
      justifyContent={"space-between"}
      alignItems={"center"}
      padding={"12px"}
      borderBottomStartRadius={{ base: "8px", md: order != 0 ? "8px" : 0 }}
      borderBottomEndRadius={{ base: "8px", md: order != 1 ? "8px" : 0 }}
      borderTopLeftRadius={{ base: "8px", md: order != 2 ? "8px" : 0 }}
      borderTopRightRadius={{ base: "8px", md: order != 3 ? "8px" : 0 }}
    >
      <Flex
        w={"80%"}
        height={"100%"}
        bgColor={" rgba(52, 52, 52, 1) "}
        borderBottomStartRadius={{ base: "8px", md: order != 0 ? "8px" : 0 }}
        borderBottomEndRadius={{ base: "8px", md: order != 1 ? "8px" : 0 }}
        borderTopLeftRadius={{ base: "8px", md: order != 2 ? "8px" : 0 }}
        borderTopRightRadius={{ base: "8px", md: order != 3 ? "8px" : 0 }}
        flexDir={"column"}
        padding={"4px 12px"}
      >
        <Heading fontSize={"0.75em"} color={"gray"}>
          {heading}
        </Heading>
        <Flex
          as={"p"}
          justifyContent={"center"}
          alignItems={"center"}
          fontSize={"0.75em"}
          color={"gray"}
          w={"100%"}
          height={"100%"}
        >
          {content}
        </Flex>
      </Flex>
      <Icon as={icon} />
    </Flex>
  );
};

const AboutUsContactList = [
  {
    heading: "ایمیل:",
    content: "content",
    icon: FiAtSign,
  },
  {
    heading: "تلگرام روشنک پیانو:",
    content: "content",

    icon: FaTelegram,
  },
  {
    heading: "شماره واتسپ :",
    content: "content",

    icon: IoLogoWhatsapp,
  },
  {
    heading: "اینستاگرام روشنک پیانو:",
    content: "content",

    icon: BiLogoInstagramAlt,
  },
];
