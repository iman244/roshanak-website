import { useEffect, useState } from "react";
import { simple_lesson } from "../types/frontTypes";
import { BsFillPlayFill, BsPauseFill } from "react-icons/bs";
import { Flex, Icon, Spinner, Text } from "@chakra-ui/react";
import Link from "next/link";
import { LinkLoading } from "./LinkLoading";

export const SimpleLesson = ({
  simple_lesson,
  href,
}: {
  simple_lesson: simple_lesson;
  href?: string;
}) => {
  const [audioCtx, setAudioCtx] = useState(new AudioContext());
  const [waitingAudio, setWaitingAudio] = useState(false);

  const [audioFile, setAudioFile] = useState(new Audio(simple_lesson.voice));
  const [pAudioPlayed, setPAudioPlayed] = useState<number>(0);
  const [audioPlay, setAudioPlay] = useState<boolean>(false);

  useEffect(() => {
    const source = audioCtx.createMediaElementSource(audioFile);
    source.connect(audioCtx.destination);

    audioFile.addEventListener("timeupdate", (ev) => {
      setPAudioPlayed(() => {
        if (
          !audioFile.duration ||
          audioFile.currentTime == audioFile.duration
        ) {
          setAudioPlay(false);
          return 0;
        }
        return (audioFile.currentTime / audioFile.duration) * 100;
      });
    });
    audioFile.addEventListener(
      "waiting",
      (ev) => {
        console.log("waiting");
        setWaitingAudio(true);
      },
      {
        once: true,
      }
    );
    audioFile.addEventListener(
      "playing",
      (ev) => {
        console.log("playing");
        setWaitingAudio(false);
      },
      {
        once: true,
      }
    );
  }, []);

  return (
    <Flex
      w={"100%"}
      height={"50px"}
      alignItems={"center"}
      justifyContent={"space-between"}
      gap={"12px"}
      bgColor={"rgba(217, 217, 217, 0.3)"}
      padding={"16px"}
      bg={`linear-gradient(90deg, #B549F6 0%,#B549F6 ${pAudioPlayed}%, rgba(217, 217, 217, 0.3) ${
        pAudioPlayed + 10
      }%, rgba(217, 217, 217, 0.3) 100%);`}
      // transition={"background 0.2s linear"}
    >
      <Flex alignItems={"center"} gap={"24px"}>
        {href ? (
          <LinkLoading href={href}>
            <Text _hover={{ textDecoration: "underline" }}>
              {simple_lesson.title}
            </Text>
          </LinkLoading>
        ) : (
          <Text>{simple_lesson.title}</Text>
        )}
        <Text fontSize={"0.75em"}>{simple_lesson.description}</Text>
      </Flex>
      <Flex
        justifyContent={"center"}
        alignItems={"center"}
        borderRadius={"50%"}
        _hover={{
          bgColor: "rgba(217, 217, 217, 0.5)",
        }}
        padding={"2px"}
        w={"fit-content"}
        h={"fit-content"}
        cursor={"pointer"}
        onClick={() => {
          if (audioPlay) {
            audioFile.pause();
          } else {
            audioCtx.resume();
            audioFile.play();
          }
          setAudioPlay(!audioPlay);
        }}
      >
        {waitingAudio ? (
          <Spinner size={"sm"} />
        ) : (
          <Icon as={audioPlay ? BsPauseFill : BsFillPlayFill} boxSize={6} />
        )}
      </Flex>
    </Flex>
  );
};
