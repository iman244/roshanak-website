import { Box, Flex } from "@chakra-ui/react";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { motion } from "framer-motion";
import { ChevronLeftIcon, ChevronRightIcon } from "@chakra-ui/icons";
import Slider from "react-slick";

export const SlideContainer = ({
  w = "300px",
  settings,
  children,
  ...rest
}: any) => {
  return (
    <Flex
      alignItems={"center"}
      justifyContent={"center"}
      padding={"10px 0"}
      {...rest}
      w={"100%"}
      overflow={"hidden"}
    >
      <Box w={w}>
        <Slider {...settings}>{children}</Slider>
      </Box>
    </Flex>
  );
};

export const NextArrow = (props: any) => {
  const {
    className,
    style,
    onClick,
    arrowIconProps,
    arrowProps,
    slideElementSize,
  } = props;
  return (
    <Box
      as={motion.div}
      className={className}
      style={{ left: "unset", right: `calc((100vw - ${slideElementSize})/2)` }}
      {...arrowProps}
      onClick={onClick}
    >
      <ChevronRightIcon {...arrowIconProps} />
    </Box>
  );
};

export const PreviousArrow = (props: any) => {
  const {
    className,
    style,
    onClick,
    arrowIconProps,
    arrowProps,
    slideElementSize,
  } = props;

  return (
    <Box
      as={motion.div}
      className={className}
      style={{ left: `calc((100vw - ${slideElementSize})/2)`, right: "unset" }}
      {...arrowProps}
      onClick={onClick}
    >
      <ChevronLeftIcon {...arrowIconProps} />
    </Box>
  );
};

export const slideItemsStyle = {
  arrowIconProps: {
    width: "100%",
    height: "100%",
    color: "white",
    opacity: "0.7",
    _hover: { opacity: 1 },
    boxSize: 10,
  },
  arrowProps: {
    _before: { display: "none" },
    top: "calc(50% - 20px)",
    transform: "none",
    width: "fit-content",
    height: "fit-content",

    zIndex: "1000",
    whileTap: {
      scale: 0.9,
    },
  },
};
