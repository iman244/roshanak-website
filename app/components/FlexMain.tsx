import { Flex } from "@chakra-ui/react";
import React from "react";

export const FlexMain = ({ children }: any) => {
  return (
    <Flex
      as={"main"}
      alignItems={"center"}
      justifyContent={"center"}
      flexDir={"column"}
      dir="rtl"
    >
      {children}
    </Flex>
  );
};
