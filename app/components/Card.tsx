import { Box, Flex } from "@chakra-ui/react";
import Image from "next/image";
import React from "react";

export const Card = ({
  children,
  imageSrc,
  bgColor = "rgba(61, 61, 61, 1)",
  w = 160,
}: {
  children: any;
  imageSrc: string;
  bgColor?: string;
  w?: number;
}) => {
  return (
    <Flex
      flexDir={"column"}
      justifyContent={"center"}
      alignItems={"center"}
      borderRadius={"12px"}
      w={`${w}px`}
      overflow={"hidden"}
    >
      <Image
        src={imageSrc}
        alt="imageSrc"
        width={w}
        height={w}
        priority={true}
      />
      <Flex bgColor={bgColor} w={"100%"}>
        {children}
      </Flex>
    </Flex>
  );
};

export const BorderCard = ({
  children,
  imageSrc,
}: {
  children: any;
  imageSrc: string;
}) => {
  return (
    <Box position={"relative"}>
      <Flex
        position={"absolute"}
        top={"-35px"}
        left={"calc(50% - 35px)"}
        width={"70px"}
        height={"70px"}
        borderRadius={"50%"}
        overflow={"hidden"}
      >
        <Image
          src={imageSrc}
          alt="imageSrc"
          width={70}
          height={70}
          priority={true}
        />
      </Flex>
      <Flex
        flexDir={"column"}
        justifyContent={"center"}
        alignItems={"center"}
        borderRadius={"24px"}
        w={"200px"}
        border={"2px solid transparent"}
        background={
          "linear-gradient(90deg, color-mix(in srgb,rgba(33, 198, 226, 1), black 40%) 0%, color-mix(in srgb,rgba(107, 136, 235, 1), black 40%) 50%, color-mix(in srgb,rgba(181, 73, 246, 1), black 40%) 100%) padding-box, linear-gradient(90deg, rgba(33, 198, 226, 1) 0%, rgba(107, 136, 235, 1) 50%, rgba(181, 73, 246, 1) 100%) border-box;"
        }
        overflow={"hidden"}
      >
        <Flex bgColor={"rgba(61, 61, 61, 1)"}>{children}</Flex>
      </Flex>
    </Box>
  );
};
