import {
  Box,
  Button,
  ChakraProps,
  Checkbox,
  Flex,
  Icon,
  Input,
  InputGroup,
  InputLeftElement,
  Spinner,
  Text,
} from "@chakra-ui/react";
import React, { Children, cloneElement, useState } from "react";
import { Controller, useForm, useFormState } from "react-hook-form";
import { FormProvider, useFormContext } from "react-hook-form";
import { useMutation } from "react-query";
import apiClient from "../http-common";
import axios, { AxiosError } from "axios";
import { GradientText } from "./TextComponenets";
import {
  POSTerrorThenFunctions,
  POSTsuccessThenFunctions,
  POSTunknownThenFunctions,
} from "../types/restTypes";
import { BiHide, BiShow } from "react-icons/bi";
import { ColorfulButton } from "./ButtonComponents";

export interface FormProps {
  defaultValues?: object;
  children?: any;
  url: string;
  onSettledF?: POSTunknownThenFunctions;
  onSubmitF?: (data: any) => any;
  case200?: POSTsuccessThenFunctions;
  case201?: POSTsuccessThenFunctions;
  case401?: POSTsuccessThenFunctions;
  caseError?: POSTerrorThenFunctions;
}

export default function Form({
  defaultValues,
  children,
  // body,
  url,
  onSettledF,
  onSubmitF,
  case200,
  case201,
  case401,
  caseError,
}: FormProps) {
  const methods = useForm({ defaultValues });
  // const [file, setFile] = useState(null);
  const { handleSubmit } = methods;
  const onSubmit = (data: any) => {
    var d = data;
    if (onSubmitF) {
      d = onSubmitF(data);
    }

    let form = new FormData();
    Object.entries(d).forEach(([field, value]: any, index) => {
      if (field == "file") {
        form.append(field, value);
      } else {
        form.append(field, value);
      }
    });

    POST.mutate(d);
  };
  const onError = (data: any) => console.log(data);

  // it retrive file from input ** i get it from my previous code, i do not use value because the value was not file itself
  // let elements = Children.toArray(children);
  // let elementsO = elements.map((elm: any) => {
  //   if (elm.props.type == "file") {
  //     return cloneElement(elm, {
  //       onChange: (e: any) => {
  //         setFile(e.target.files[0]);
  //       },
  //     });
  //   } else {
  //     return elm;
  //   }
  // });

  const POST = useMutation(
    async (data) => {
      return await apiClient.post(url, data);
    },
    {
      onSettled(data, error, variables, context) {
        onSettledF && onSettledF(data, error, variables, context);
      },
      onSuccess(d, variables, context): any {
        const { status } = d;
        const data = d.data.data;
        console.log("this is success");
        switch (status) {
          case 200:
            case200 && case200(data, d, variables, context);
            break;
          case 201:
            case201 && case201(data, d, variables, context);
            break;
          case 401:
            case401 && case401(data, d, variables, context);
            break;
          default:
            break;
        }
      },
      onError(error: any, variables, context) {
        if (axios.isAxiosError(error)) {
          const { response } = error;
          const d = response!.data;
          const { status, data } = d;
          switch (status) {
            case 401:
              case401 && case401(data, d, variables, context);
              break;
            default:
              break;
          }
        }

        caseError && caseError(error, variables, context);
      },
    }
  );

  return (
    <FormProvider {...methods}>
      <Box as="form" w={"100%"} onSubmit={handleSubmit(onSubmit, onError)}>
        {children}
      </Box>
    </FormProvider>
  );
}

export const ConnectForm = ({ children }: any) => {
  const methods = useFormContext();

  return children({ ...methods });
};

export const ComplicatedInput = ({
  register,
  name,
  placeholder,
  rules,
  type = "text",
}: any) => {
  const { control } = useFormContext();
  const { errors } = useFormState();
  return (
    <Flex flexDir={"column"} gap={"4px"} w={"100%"}>
      <GradientText>
        {errors[name] ? `${errors[name]?.message}` : ""}
      </GradientText>
      <Controller
        control={control}
        name={name}
        rules={rules}
        render={({ field: { value, onChange, ...field } }) => {
          if (type == "file") {
            return (
              <FormInputUI
                register={register}
                placeholder={placeholder}
                type={type}
                value={value?.fileName}
                onChange={(event: any) => {
                  onChange(event.target.files[0]);
                }}
                {...field}
              />
            );
          }
          return (
            <FormInputUI
              register={register}
              placeholder={placeholder}
              type={type}
              value={value}
              onChange={onChange}
              {...field}
            />
          );
        }}
      />
    </Flex>
  );
};

const FormInputUI = (props: any) => {
  return (
    <Input
      {...props}
      autoComplete={"off"}
      border={"2px solid transparent"}
      background={
        "linear-gradient(90deg, rgba(52, 52, 52, 1) 0% , rgba(52, 52, 52, 1) 100%) padding-box,linear-gradient(90deg, rgba(152, 152, 152, 1) 0% , rgba(152, 152, 152, 1) 100%) border-box;"
      }
      fontSize={"0.75em"}
      transition={"all 300ms"}
      _placeholder={{
        fontSize: "0.75em",
        color: "white",
        fontWeight: "300",
      }}
      _hover={{
        borderColor: "",
      }}
      _focusVisible={{
        boxShadow: "",
        border: "2px solid transparent",
        background:
          "linear-gradient(90deg, rgba(52, 52, 52, 1) 0% , rgba(52, 52, 52, 1) 100%) padding-box,linear-gradient(90deg, #21C6E2 0%, #6B88EB 45.29%, #B549F6 99.95%) border-box;",
      }}
    />
  );
};

interface AuthenticateFormLayoutProps {
  children: any;
  heading: string;
}

export const AuthenticateFormLayout = ({
  children,
  heading,
}: AuthenticateFormLayoutProps) => {
  return (
    <Flex
      flexDir={"column"}
      border={"2px solid transparent"}
      borderRadius={"12px"}
      background={
        "linear-gradient(90deg, rgba(52, 52, 52, 1) 0% , rgba(52, 52, 52, 1) 100%) padding-box,linear-gradient(90deg, #21C6E2 0%, #6B88EB 45.29%, #B549F6 99.95%) border-box;"
      }
      overflow={"hidden"}
    >
      <Flex
        position={"absolute"}
        left={"50%"}
        transform={"translateX(-50%)"}
        top={"60px"}
        background={
          "linear-gradient(90deg, rgba(52, 52, 52, 1) 0% , rgba(52, 52, 52, 1) 100%) padding-box,linear-gradient(90deg, #21C6E2 0%, #6B88EB 45.29%, #B549F6 99.95%) border-box;"
        }
        border={"2px solid transparent"}
        height={"40px"}
        overflow={"hidden"}
        alignItems={"center"}
        justifyContent={"center"}
        padding={"4px 24px"}
      >
        {heading}
      </Flex>
      <Flex
        borderTop={"2px solid transparent"}
        background={
          "linear-gradient(90deg, rgba(52, 52, 52, 1) 0% , rgba(52, 52, 52, 1) 100%) padding-box,linear-gradient(90deg, #21C6E2 0%, #6B88EB 45.29%, #B549F6 99.95%) border-box;"
        }
        mt={"40px"}
        paddingTop={"32px"}
        flexDir={"column"}
        alignItems={"center"}
      >
        {children}
      </Flex>
    </Flex>
  );
};

export const InternalFormLayout = ({ children }: any) => {
  return (
    <Flex
      flexDir={"column"}
      alignItems={"center"}
      justifyContent={"space-between"}
      w={{ base: "80vw", sm: "70vw", md: "700px" }}
      // minH={{ base: "200px", sm: "250px" }}
      padding={"24px"}
      paddingTop={"0px"}
      // gap={"32px"}
    >
      {children}
    </Flex>
  );
};

interface FormLayout extends FormProps {
  children?: any;
  heading: string;
}

export const FormLayout = ({ children, heading, ...rest }: FormLayout) => {
  return (
    <AuthenticateFormLayout heading={heading}>
      <Form {...rest}>
        <InternalFormLayout>{children}</InternalFormLayout>
      </Form>
    </AuthenticateFormLayout>
  );
};

export const NameInput = () => {
  return (
    <ConnectForm>
      {({ register }: any) => (
        <ComplicatedInput
          register={register}
          name={"first_name"}
          placeholder={"نام"}
          rules={{
            required: {
              value: true,
              message: "لطفاً نام خود را وارد کنید",
            },
          }}
        />
      )}
    </ConnectForm>
  );
};

export const LastNameInput = () => {
  return (
    <ConnectForm>
      {({ register }: any) => (
        <ComplicatedInput
          register={register}
          name={"last_name"}
          placeholder={"نام خانوادگی"}
          rules={{
            required: {
              value: true,
              message: "لطفاً نام خانوادگی خود را وارد کنید",
            },
          }}
        />
      )}
    </ConnectForm>
  );
};

export const EmailInput = () => {
  return (
    <ConnectForm>
      {({ register }: any) => (
        <ComplicatedInput
          register={register}
          name={"email"}
          placeholder={"ایمیل"}
          rules={{
            required: {
              value: true,
              message: "لطفاً ایمیل خود را وارد کنید",
            },
            pattern: {
              value: /^\S+@\S+$/i,
              message: "ایمیل صحیح نیست",
            },
          }}
        />
      )}
    </ConnectForm>
  );
};

export const PasswordInput = () => {
  const [show, setShow] = useState(false);

  return (
    <ConnectForm>
      {({ register }: any) => (
        <InputGroup size="md">
          <ComplicatedInput
            register={register}
            name="password"
            placeholder="رمز عبور"
            type={show ? "text" : "password"}
            rules={{
              required: {
                value: true,
                message: "لطفاً رمز عبور خود را وارد کنید",
              },
              minLength: {
                value: 4,
                message: "رمز عبور باید حداقل 4 حرف باشد",
              },
            }}
          />
          <InputLeftElement
            width="3rem"
            top={"28px"}
            _focusVisible={{
              outline: "none",
            }}
          >
            <Button
              size="sm"
              onClick={() => setShow(!show)}
              variant={"link"}
              color={"white"}
            >
              {show ? <Icon as={BiHide} /> : <Icon as={BiShow} />}
            </Button>
          </InputLeftElement>
        </InputGroup>
      )}
    </ConnectForm>
  );
};

export const SignUpLaw = () => {
  const { control } = useFormContext();
  const { errors } = useFormState();

  return (
    <Flex bgColor={"#3D3D3D"} padding={"8px"} mt={"28px"}>
      <ConnectForm>
        {({ register }: any) => (
          <Controller
            name={"checkbox"}
            control={control}
            rules={{
              validate: {
                checkTrue: (v) => v == true,
                // || "برای ثبت نام در سایت باید قوانین عضویت را بپذیرید ",
              },
            }}
            render={({ field }) => (
              <Checkbox
                {...register}
                {...field}
                size={"sm"}
                isInvalid={errors["checkbox"] ? true : false}
                color={errors["checkbox"] ? "red" : "white"}
              >
                قوانین عضویت در سایت را مطالعه کرده و می‌پذیرم.
              </Checkbox>
            )}
          />
        )}
      </ConnectForm>
    </Flex>
  );
};

export const PhoneNumberInput = () => {
  return (
    <ConnectForm>
      {({ register }: any) => (
        <InputGroup size="md">
          <ComplicatedInput
            register={register}
            type={"tel"}
            name="phone_number"
            placeholder="شماره همراه"
            rules={{
              required: {
                value: true,
                message: "لطفاً شماره همراه خود را وارد کنید",
              },
              pattern: {
                value: /^[0-9]{10}$/g,
                message: "شماره همراه صحیح نیست",
              },

              maxLength: { value: 10, message: "شماره همراه باید 10 رقم باشد" },
              minLength: { value: 10, message: "شماره همراه باید 10 رقم باشد" },
            }}
          />
          <InputLeftElement
            width="3rem"
            top={"28px"}
            _focusVisible={{
              outline: "none",
            }}
            dir="ltr"
            borderTopLeftRadius={"4px"}
            borderBottomLeftRadius={"4px"}
          >
            <Text
              bgGradient="linear-gradient(90deg, #21C6E2 0%, #6B88EB 45.29%, #B549F6 99.95%);"
              bgClip="text"
            >
              +98
            </Text>
          </InputLeftElement>
        </InputGroup>
      )}
    </ConnectForm>
  );
};

export const VerficationCodeInput = () => {
  return (
    <ConnectForm>
      {({ register }: any) => (
        <ComplicatedInput
          register={register}
          name={"verfication-code"}
          placeholder={"کد تأیید"}
          rules={{
            required: {
              value: true,
              message: "لطفاً کد تأیید ارسال شده به ایمیل خود را وارد کنید",
            },
            pattern: {
              value: /^[0-9]*$/g,
              message: "کد تأیید صحیح نیست",
            },
          }}
        />
      )}
    </ConnectForm>
  );
};

export const ProfileImageInput = () => {
  return (
    <ConnectForm>
      {({ register }: any) => (
        <ComplicatedInput
          type={"file"}
          register={register}
          name={"image"}
          placeholder={"عکس"}
          rules={{
            required: {
              value: true,
              message: "عکس پروفایل خود را آپلود کنید",
            },
          }}
        />
      )}
    </ConnectForm>
  );
};

export const SubmitButton = ({
  children = "ارسال",
  isloading,
}: {
  children?: any;
  isloading: boolean;
}) => {
  return (
    <Flex alignItems={"flex-end"} justifyContent={"flex-end"} h={"100%"}>
      <ColorfulButton type="submit">
        <Flex
          alignItems={"center"}
          justifyContent={"center"}
          minW={"24px"}
          textAlign={"center"}
        >
          {isloading ? (
            <Spinner color="rgba(200,200,200,1)" size="xs" />
          ) : (
            children
          )}
        </Flex>
      </ColorfulButton>
    </Flex>
  );
};
