"use client";

import {
  Button,
  Flex,
  Grid,
  GridItem,
  Heading,
  Icon,
  Text,
} from "@chakra-ui/react";
import React, { useContext } from "react";
import { LanguageContext } from "../contexts/LanguageContext";
import Image from "next/image";
import Link from "next/link";
import { FaTelegram, FaWhatsapp, FaInstagram } from "react-icons/fa";

export default function Footer() {
  const { language } = useContext(LanguageContext);
  return (
    <footer className="page-footer">
      <Flex flexDir={"column"} alignItems={"center"} justifyContent={"center"}>
        <Grid
          templateColumns={{ base: "repeat(1, 1fr)", sm: "repeat(3, 1fr)" }}
          dir={language == "fa" ? "rtl" : "ltr"}
          padding={"32px"}
          paddingBottom={"12px"}
          w={"100%"}
          maxW={"1000px"}
          gap={"48px"}
          justifyItems={"center"}
        >
          <GridItem>
            <Flex flexDir={"column"} gap={4}>
              <Image
                alt="logo"
                src="/icon-blackAndwhite.png"
                width={80}
                height={80}
              />
              <Text fontSize={"0.75em"} textAlign={"center"}>
                روشنک پیانو
              </Text>
            </Flex>
          </GridItem>
          <GridItem>
            <FotterVerticalRow
              heading={"دسترسی سریع:"}
              list={[
                { text: "خانه", href: "" },
                { text: "ورود", href: "" },
                { text: "تماس با ما", href: "" },
                { text: "سوالات متداول", href: "" },
              ]}
            />
          </GridItem>
          <GridItem>
            <FotterVerticalRow
              heading={"اطلاعات تماس"}
              list={[
                { text: "تلفن:", href: "" },
                { text: "آدرس:", href: "" },
                { text: "ایمیل:", href: "" },
              ]}
            />
          </GridItem>

          <GridItem></GridItem>
          <GridItem order={{ base: "1", sm: 0 }}>
            <Flex gap={"12px"}>
              <Image
                alt="samandehiLogo"
                src="/samandehiLogo.png"
                width={60}
                height={60}
              />
              <Image
                alt="eNamadLogo"
                src="/eNamadLogo.png"
                width={60}
                height={60}
              />
            </Flex>
          </GridItem>
          <GridItem>
            <Flex gap={"8px"} alignItems={"flex-end"} h={"100%"} w={"100%"}>
              <Link href={"it-doesnt-defined-yet"}>
                <Icon as={FaTelegram} boxSize={6} />
              </Link>
              <Link href={"it-doesnt-defined-yet"}>
                <Icon as={FaWhatsapp} boxSize={6} />
              </Link>
              <Link href={"it-doesnt-defined-yet"}>
                <Icon as={FaInstagram} boxSize={6} />
              </Link>
            </Flex>
          </GridItem>
        </Grid>
        <Text
          backgroundColor={"rgba(52, 52, 52, 1)"}
          textAlign={"center"}
          fontWeight={300}
          fontSize={"x-small"}
          dir="rtl"
          padding={"8px"}
          w={"100%"}
        >
          کلیه حقوق این سایت متعلق به آموزشگاه روشنک پیانو می باشد و ...
        </Text>
      </Flex>
    </footer>
  );
}

const FotterVerticalRow = ({
  heading,
  list,
}: {
  heading: string;
  list: { text: string; href: string }[];
}) => {
  return (
    <Flex flexDir={"column"} gap={"12px"}>
      <Heading fontSize={"1em"} fontWeight={"500"}>
        {heading}
      </Heading>
      {list.map((item) => (
        <Link href={item.href} style={{ width: "fit-content" }}>
          <Text
            opacity={"0.8"}
            _hover={{ textDecoration: "underline" }}
            fontSize={"0.75em"}
            fontWeight={400}
          >
            {item.text}
          </Text>
        </Link>
      ))}
    </Flex>
  );
};
