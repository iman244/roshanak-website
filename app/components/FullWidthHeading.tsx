import { Flex } from "@chakra-ui/react";
import React from "react";

export const FullWidthHeading = ({ children }: any) => {
  return (
    <Flex
      bgColor={"rgba(61, 61, 61, 1)"}
      paddingY={"12px"}
      justifyContent={"center"}
      alignItems={"center"}
      width={"100%"}
    >
      {children}
    </Flex>
  );
};
