import { Button } from "@chakra-ui/react";
import { motion } from "framer-motion";
import React from "react";

export const ColorfulButton = ({ children, ...rest }: any) => {
  return (
    <Button
      {...rest}
      padding={"16px 24px !important"}
      height={"24px"}
      borderRadius={"16px"}
      textAlign={"center"}
      fontSize={"sm"}
      fontWeight={300}
      _hover={{ bgColor: "" }}
      _active={{ bgColor: "" }}
      color={"white"}
      display={"flex"}
      justifyContent={"center"}
      alignItems={"center"}
      bgGradient="linear(to-l,  #B549F6 0%, #6B88EB 50%, #21C6E2 100%)"
      cursor={false ? "not-allowed" : "pointer"}
      lineHeight={"base"}
    >
      {children}
    </Button>
  );
};
