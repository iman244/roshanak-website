import { Box, Flex, StyleProps } from "@chakra-ui/react";
import { motion } from "framer-motion";
import Link from "next/link";
import { CSSProperties } from "react";

export const Banner = (props: any) => {
  const {
    children,
    ...rest
  }: {
    children: any;
    rest?: StyleProps;
  } = props;
  return (
    <BoxPiano variant={"banner"} width={"100%"} height={"150px"} {...rest}>
      {children}
    </BoxPiano>
  );
};

export const BoxPiano = (props: any) => {
  const {
    children,
    href = "",
    liquid = false,
    animationDuration = 5,
    backgroundLiquid = "red",
    backgroundBALiquid = "rgb(20,20,20)",
    variant,
    contentStyle,
    boxShadow = true,
    tapAnimation = true,
    ...rest
  }: {
    children: any;
    liquid: boolean;
    animationDuration: number;
    backgroundLiquid: string;
    backgroundBALiquid: string;
    variant: string;
    boxShadow: boolean;
    href?: string;
    rest?: StyleProps;
    contentStyle?: StyleProps;
    tapAnimation?: boolean;
  } = props;
  const styleLiquid: CSSProperties = { top: "20px", backgroundColor: "#000" };
  return (
    <Box
      as={motion.div}
      {...rest}
      backgroundPosition={"center"}
      style={{
        boxShadow: boxShadow ? "0px 4px 7px rgba(0, 0, 0, 0.4)" : "",
        cursor: "pointer",
        borderRadius: "16px",
        overflow: "hidden",
        backgroundSize: variant == "banner" ? "cover" : "contain",
      }}
      whileTap={{
        scale: tapAnimation ? (variant == "banner" ? 0.95 : 0.9) : 1,
      }}
    >
      {href ? (
        <Link
          href={href}
          style={{
            width: "100%",
            height: "100%",
            position: "relative",
          }}
        >
          <Flex
            justifyContent={"center"}
            alignItems={"center"}
            width={"100%"}
            height={"100%"}
            zIndex={2}
            position={"relative"}
            {...contentStyle}
          >
            {children}
          </Flex>
          {liquid && (
            <Box
              className="liquid"
              zIndex={0}
              background={backgroundLiquid}
              _before={{
                ...styleLiquid,
                background: backgroundBALiquid,
                opacity: "1",
                animationDuration: `${animationDuration}s`,
              }}
              _after={{
                ...styleLiquid,
                background: backgroundBALiquid,
                opacity: "0.5",
                animationDuration: `${animationDuration + 5}s`,
              }}
            ></Box>
          )}
        </Link>
      ) : (
        <Flex
          justifyContent={"center"}
          alignItems={"center"}
          width={"100%"}
          height={"100%"}
          zIndex={2}
          position={"relative"}
          {...contentStyle}
        >
          {children}
        </Flex>
      )}
    </Box>
  );
};
