import { Box, Flex, Icon, Text } from "@chakra-ui/react";
import Link from "next/link";
import React, { useContext } from "react";
import { LanguageContext } from "../contexts/LanguageContext";
import { IconType } from "react-icons";
import Image from "next/image";

export const InstallLink = ({
  href,
  text,
  src,
}: {
  href: string;
  text: string;
  src: string;
}) => {
  const { language } = useContext(LanguageContext);
  return (
    <Link href={href} style={{ width: "fit-content", display: "inline-block" }}>
      <Flex
        padding={"18px 12px"}
        justifyContent={"space-between"}
        alignItems={"center"}
        w={"180px"}
        dir={language == "fa" ? "rtl" : "ltr"}
        border={"2px solid transparent"}
        borderRadius={"12px"}
        background={
          "linear-gradient(90deg, color-mix(in srgb,#21C6E2, black 40%) 0%, color-mix(in srgb,#6B88EB, black 40%) 50%, color-mix(in srgb,#B549F6, black 40%) 100%) padding-box,linear-gradient(90deg, #21C6E2 0%, #6B88EB 45.29%, #B549F6 99.95%) border-box;"
        }
      >
        <Text>{text}</Text>
        <Flex
          alignItems={"center"}
          justifyContent={"center"}
          w={"30px"}
          height={"30px"}
        >
          <Image alt="icon" src={src} width={30} height={30} />
        </Flex>
      </Flex>
    </Link>
  );
};

("linear-gradient(90deg, color-mix(in srgb,#21C6E2, black 40%) 0%, color-mix(in srgb,#6B88EB, black 40%) 25%, color-mix(in srgb,#B549F6, black 40%) 50%) padding-box,linear-gradient(90deg, #21C6E2 0%, #6B88EB 25%, #B549F6 50%) border-box;");

// background: linear-gradient(90deg, rgba(33, 198, 226, 0.2) 0%, rgba(107, 136, 235, 0.2) 45.29%, rgba(181, 73, 246, 0.2) 99.95%),
// linear-gradient(90deg, #21C6E2 0%, #6B88EB 45.29%, #B549F6 99.95%);
