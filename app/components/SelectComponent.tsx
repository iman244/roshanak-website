import { useState, useEffect } from "react";
import Select from "react-select";

export const SelectComponenet = ({
  selectedOption,
  setSelectedOption,
  text,
  options,
  isClearable = true,
  onChange,
  value,
}: any) => {
  return (
    <Select
      value={value}
      isClearable={isClearable}
      defaultValue={selectedOption}
      onChange={(v) => {
        onChange && onChange(v);
        setSelectedOption(v);
      }}
      options={options}
      placeholder={text}
      styles={SelectStyle}
      noOptionsMessage={(inputValue) => {
        return <>موردی یافت نشد!</>;
      }}
    />
  );
};

export const SelectStyle = {
  control: (styles: any, state: any) => ({
    ...styles,
    backgroundColor: "transparent",
    width: "100%",

    border: "2px solid transparent",
    "&:hover": { borderColor: "transparent" },
  }),
  singleValue: (styles: any) => ({
    ...styles,
    color: "white !important",
  }),
  dropdownIndicator: (styles: any) => ({
    ...styles,
    color: "white !important",
    "&:hover": {
      opacity: "0.5",
      cursor: "pointer",
    },
  }),
  clearIndicator: (styles: any) => ({
    ...styles,
    color: "white !important",
    "&:hover": {
      opacity: "0.5",
      cursor: "pointer",
    },
  }),

  option: (styles: any, state: any) => ({
    ...styles,
    fontSize: "0.75em",
    // paddingRight: "20px",
    color: "white",
    backgroundColor: "transparent",
    //   ? "color-mix(in srgb, gray 60%, black 40%)"
    //   : "color-mix(in srgb, gray 60%, black 10%)",
    opacity: state.isSelected ? 1 : 0.5,
    "&:hover": {
      opacity: 1,
      cursor: "pointer",
    },

    ":active": {
      backgroundColor: "#b02eff",
    },
  }),
  menu: (base: any) => ({
    ...base,
    zIndex: "1000",
    background:
      "linear-gradient(0deg, #21C6E2 0%, #6B88EB 45.29%, #B549F6 99.95%);",
  }),
  indicatorSeparator: (styles: any, state: any) => ({
    ...styles,
    display: "none",
  }),
  placeholder: (styles: any, state: any) => ({
    ...styles,
    color: "white",
    fontSize: "0.8em",
  }),
};
