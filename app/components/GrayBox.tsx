import { Flex } from "@chakra-ui/react";

export const GrayBox = ({ children }: any) => {
  return (
    <Flex
      justifyContent={"center"}
      alignItems={"center"}
      w={{ base: "50vw", sm: "30vw" }}
      height={{ base: "75vw", sm: "45vw" }}
      bgColor={"rgba(217, 217, 217, 0.1)"}
      maxW={"250px"}
      maxH={"350px"}
    >
      {children}
    </Flex>
  );
};
