import React from "react";
import { Flex, Spinner } from "@chakra-ui/react";

const Suspense = ({
  isloading,
  children,
  contentContainer = true,
  fallback = (
    <Flex
      height={"100%"}
      justifyContent={"center"}
      alignItems={"center"}
      w={"100%"}
      flex={1}
    >
      <Spinner size="lg" />
    </Flex>
  ),
}: {
  isloading: boolean;
  contentContainer?: boolean;
  children: any;
  fallback?: any;
}) => {
  return (
    <>
      {contentContainer ? (
        <>{isloading ? fallback : children}</>
      ) : (
        <>{isloading ? fallback : children}</>
      )}
    </>
  );
};

export default Suspense;
