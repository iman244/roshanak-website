import { Button, Icon } from "@chakra-ui/react";
import { motion } from "framer-motion";
import { useRouter } from "next/navigation";
import React from "react";
import { HiOutlineLogout } from "react-icons/hi";

export const LogoutButton = () => {
  const router = useRouter();
  return (
    <Button
      dir="ltr"
      variant="ghost"
      rightIcon={<Icon as={HiOutlineLogout} boxSize={5} />}
      _hover={{ background: "none", color: "gray" }}
      gap={"4px"}
      as={motion.button}
      whileTap={{
        scale: 0.8,
      }}
      color={"white"}
      onClick={() => {
        localStorage.clear();
        router.push("/authenticate/sign-in");
      }}
    >
      خروج از حساب
    </Button>
  );
};
