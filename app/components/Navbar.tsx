"use client";

import {
  Box,
  Button,
  Divider,
  Drawer,
  DrawerBody,
  DrawerCloseButton,
  DrawerContent,
  DrawerFooter,
  DrawerHeader,
  DrawerOverlay,
  Flex,
  Icon,
  Input,
  Popover,
  PopoverArrow,
  PopoverBody,
  PopoverContent,
  PopoverTrigger,
  Text,
  useDisclosure,
} from "@chakra-ui/react";
import Link from "next/link";
import React, { useContext } from "react";
import { LanguageContext } from "../contexts/LanguageContext";
import { BiSearch } from "react-icons/bi";
import Image from "next/image";
import { HamburgerIcon } from "@chakra-ui/icons";
import { motion } from "framer-motion";
import { BsInfoCircle } from "react-icons/bs";
import { GoPackage } from "react-icons/go";
import ProfileContainer from "../Containers/ProfileContainer";
import { NavbarProfileBadgePresenter } from "../Presenters/NavbarProfileBadgePresenter";
import { LogoutButton } from "./LogoutButton";

export default function Navbar() {
  const { language } = useContext(LanguageContext);
  const {
    isOpen: isOpenDrawer,
    onOpen: onOpenDrawer,
    onClose: onCloseDrawer,
  } = useDisclosure();

  return (
    <nav className="topNavbar">
      <Flex
        dir={language == "fa" ? "rtl" : "ltr"}
        w={"100%"}
        h={"100%"}
        paddingX={{ base: "12px", sm: "40px" }}
        alignItems={"center"}
        justifyContent={"space-between"}
      >
        <Flex alignItems={"center"} gap={"32px"}>
          <HamburgerIcon
            boxSize={"5"}
            color={"#ffffff"}
            onClick={onOpenDrawer}
            cursor={"pointer"}
            display={{ base: "inline-block", md: "none" }}
          />
          <Link href={"/"} style={{ width: "24px" }}>
            <Image
              alt="logo"
              src="/icon-blackAndwhite.png"
              width={24}
              height={24}
            />
          </Link>
          <Divider
            orientation="vertical"
            height={"20px !important"}
            display={{ base: "none", md: "block" }}
          />
          <Flex
            className="fade-container"
            gap={"32px"}
            display={{ base: "none", md: "flex" }}
          >
            {[
              {
                text: "خانه",
                href: "/",
              },
              {
                text: "دوره ها",
                href: "/packages",
              },
              // {
              //   text: "اشتراک",
              //   href: "/subscription",
              // },
              {
                text: "درباره ما",
                href: "/about-us",
              },
            ].map((item) => (
              <Box className="fade-item">
                <Link href={item.href}>
                  <Text>{item.text}</Text>
                </Link>
              </Box>
            ))}
          </Flex>
        </Flex>
        <Flex alignItems={"center"} gap={"16px"} height={"100%"}>
          <ProfileContainer>
            <NavbarProfileBadgePresenter />
          </ProfileContainer>
          <PopoverContainer />
        </Flex>
      </Flex>
      <Sidebar isOpenDrawer={isOpenDrawer} onCloseDrawer={onCloseDrawer} />
    </nav>
  );
}

const Sidebar = ({ isOpenDrawer, onCloseDrawer }: any) => {
  return (
    <Drawer isOpen={isOpenDrawer} placement="right" onClose={onCloseDrawer}>
      <DrawerOverlay />

      <DrawerContent
        dir="rtl"
        background={
          "linear-gradient(90deg, rgba(33, 198, 226, 0.5) 0%, rgba(107, 136, 235, 0.5) 45.29%, rgba(181, 73, 246,0.5) 99.95%);"
        }
        backdropFilter={"blur(12.5px)"}
        color={"white"}
      >
        <DrawerCloseButton left={"0.75em"} right={"none"} />
        <DrawerHeader>
          <Link href={"/"} onClick={onCloseDrawer}>
            روشنک پیانو
          </Link>
        </DrawerHeader>

        <DrawerBody>
          <Flex flexDir={"column"}>
            {[
              {
                icon: GoPackage,
                href: "/packages",
                text: "دوره ها",
              },
              // { icon: IoMdSettings, href: "/profile", text: "پروفایل" },
              { icon: BsInfoCircle, href: "/about-us", text: "درباره ما" },
            ].map((btn, index, array) => (
              <Link href={btn.href} key={index}>
                <Button
                  as={motion.button}
                  whileTap={{
                    scale: 0.8,
                  }}
                  leftIcon={<Icon as={btn.icon} boxSize={5} />}
                  variant="ghost"
                  _hover={{ background: "none" }}
                  w={"fit-content"}
                  gap={"4px"}
                  onClick={onCloseDrawer}
                  color={"white"}
                >
                  {btn.text}
                </Button>
              </Link>
            ))}
          </Flex>
        </DrawerBody>

        <DrawerFooter dir="ltr">
          <LogoutButton />
        </DrawerFooter>
      </DrawerContent>
    </Drawer>
  );
};

const PopoverContainer = () => {
  return <PopoverPresenter />;
};

const PopoverPresenter = () => {
  return (
    <Popover placement="bottom-end">
      <PopoverTrigger>
        <button style={{ width: "32px", height: "32px" }}>
          <Icon
            w={"32px"}
            h={"32px"}
            padding={"8px"}
            borderRadius={"50%"}
            as={BiSearch}
            cursor={"pointer"}
            _hover={{ bgColor: "rgba(255,255,255,0.3)" }}
          />
        </button>
      </PopoverTrigger>
      <PopoverContent color="black" w={"fit-content"}>
        <PopoverArrow />
        <PopoverBody>
          <Flex
            justifyContent={"space-between"}
            alignItems={"center"}
            gap={"32px"}
          >
            <Input
              padding={0}
              height={"20px"}
              fontSize={"0.675em"}
              _placeholder={{
                fontSize: "0.875em",
                fontWeight: "600",
                // className="text-gradient"
                bgGradient:
                  "linear-gradient(90deg, #21C6E2 0%, #6B88EB 45.29%, #B549F6 99.95%);",
                bgClip: "text",
              }}
              _focusVisible={{
                boxShadow: "",
              }}
              border={"none"}
              placeholder=" جستجو بین دوره ها و فایل ها و ..."
            />
            {/* جستجو بین دوره ها و فایل ها و ...
            </Text> */}
            <Image
              src={"/searchColorful.png"}
              width={12}
              height={12}
              alt="search-icon"
            />
          </Flex>
        </PopoverBody>
      </PopoverContent>
    </Popover>
  );
};
