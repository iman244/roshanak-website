import {
  Button,
  ChakraProps,
  Flex,
  FlexProps,
  Spinner,
  StyleProps,
  SystemStyleObject,
} from "@chakra-ui/react";
import Link from "next/link";
import React, { CSSProperties, useRef, useState } from "react";
import { Card } from "./Card";

interface Props extends ChakraProps {
  href: string;
  children: any;
  style?: CSSProperties;
}

export const LinkLoading = ({ href, children, style, ...rest }: Props) => {
  const [loading, setLoading] = useState(false);
  return (
    <Flex alignItems={"center"} justifyContent={"center"} gap={"6px"} {...rest}>
      {loading && <Spinner size={"xs"} />}
      <Link
        style={{ width: "100%", ...style }}
        href={href}
        onClick={() => {
          setLoading(true);
        }}
      >
        {children}
      </Link>
    </Flex>
  );
};

export const LoadingButton = ({ href, children, style, ...rest }: Props) => {
  const [loading, setLoading] = useState(false);

  return (
    <Link
      href={href}
      onClick={() => {
        setLoading(true);
      }}
    >
      <Button {...rest} gap={"6px"}>
        {loading && <Spinner size={"xs"} />}
        {children}
      </Button>
    </Link>
  );
};

export const CardLoading = ({
  href,
  imageSrc,
  children,
}: {
  href: string;
  imageSrc: string;
  children?: any;
}) => {
  const [loading, setLoading] = useState(false);
  return (
    <Link
      href={href}
      style={{ position: "relative", overflow: "hidden" }}
      onClick={() => {
        setLoading(true);
      }}
    >
      {loading && <LoadingOverlay />}
      <Card imageSrc={imageSrc}>{children}</Card>
    </Link>
  );
};

const LoadingOverlay = () => {
  return (
    <Flex
      position={"absolute"}
      w={"100%"}
      height={"100%"}
      zIndex={10}
      borderRadius={"12px"}
      bgColor={"rgba(160,160,160,0.3)"}
      justifyContent={"center"}
      alignItems={"center"}
      cursor={"not-allowed"}
    >
      <Spinner />
    </Flex>
  );
};
