import { Flex, Heading, Text, TypographyProps } from "@chakra-ui/react";
import React, { useRef, useState } from "react";

export const GradientText = ({ children, form = true }: any) => {
  return (
    <Text
      fontSize={"small"}
      height={form ? "24px" : "fit-content"}
      bgGradient="linear-gradient(90deg, #6B88EB 0%, #B549F6 100%);"
      bgClip="text"
    >
      {children}
    </Text>
  );
};

export const InfoBox = ({
  children,
  heading,
  headingAlign = "start",
}: {
  children: any;
  heading: string;
  headingAlign?: CanvasTextAlign;
}) => {
  return (
    <Flex
      w={"100%"}
      flexDir={"column"}
      gap={"24px"}
      bgColor={"rgba(217, 217, 217, 0.1)"}
      padding={"24px"}
    >
      <Heading fontWeight={"400"} fontSize={"1.25em"} textAlign={headingAlign}>
        {heading}
      </Heading>
      {children}
    </Flex>
  );
};
