import "./globals.css";
import { Vazirmatn } from "next/font/google";
import Providers from "./provider";
import Navbar from "./components/Navbar";
import Footer from "./components/Footer";
import { Flex, Spacer } from "@chakra-ui/react";
import { LanguageContextWrapper } from "./contexts/LanguageContext";

const vazirmatn = Vazirmatn({ subsets: ["arabic"] });

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={vazirmatn.className}>
        <Providers>
          <LanguageContextWrapper>
            <Flex flexDir={"column"} minH={"calc( 100vh - 66px )"} mt={"65px"}>
              <Navbar />
              {children}
              <Spacer />
              <Footer />
            </Flex>
          </LanguageContextWrapper>
        </Providers>
      </body>
    </html>
  );
}
