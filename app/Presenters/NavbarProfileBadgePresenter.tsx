import React from "react";
import { query } from "../types/frontTypes";
import Suspense from "../components/Suspense";
import { backend401, backend_profile } from "../types/backendTypes";
import Link from "next/link";
import { Flex, Spinner } from "@chakra-ui/react";

export const NavbarProfileBadgePresenter = ({
  query: profileQuery = { data: undefined, status: "loading" },
}: {
  query?: query<backend_profile | backend401 | undefined>;
}) => {
  console.log(profileQuery);
  return (
    // <Flex height={"100% !important"}>
    <Suspense
      isloading={profileQuery.status == "loading"}
      fallback={
        <Flex
          height={"100%"}
          justifyContent={"center"}
          alignItems={"center"}
          w={"100%"}
          flex={1}
        >
          <Spinner size="xs" />
        </Flex>
      }
    >
      {profileQuery.status == "success" ? (
        <Link href="/profile" style={{ height: "100%" }}>
          <Flex
            height={"100%"}
            alignItems={"center"}
            justifyContent={"center"}
            _hover={{ textDecoration: "underline" }}
            fontSize={"0.75em"}
          >
            {profileQuery.data?.email}
          </Flex>
        </Link>
      ) : (
        <Link href={"/authenticate/sign-in"}>ورود/ثبت نام</Link>
      )}
    </Suspense>
    // </Flex>
  );
};
