"use client";

import React, { useState } from "react";
import { polished_package_noVideo, query } from "../types/frontTypes";
import Suspense from "../components/Suspense";
import {
  Button,
  Flex,
  Grid,
  GridItem,
  Heading,
  SimpleGrid,
  Text,
} from "@chakra-ui/react";
import { Card } from "../components/Card";
import { LoadingButton } from "../components/LinkLoading";

export default function PackagesPresenter({
  query: packagesQuery = { data: [], status: "loading" },
}: {
  query?: query<polished_package_noVideo[]>;
}) {
  const [category, setCategory] = useState("All");
  console.log(packagesQuery);
  const itemsToShow = packagesQuery.data.filter((predicate) => {
    if (category == "All") return true;
    return predicate.category == category;
  }).length;
  return (
    <>
      <Heading fontWeight={400} fontSize={"1.25em"} mb={"24px"}>
        دسته بندی دوره ها
      </Heading>
      <Flex
        padding={"2px"}
        w={"fit-content"}
        bgGradient={
          "linear-gradient(90deg, #21C6E2 0%, #6B88EB 45.29%, #B549F6 99.95%);"
        }
        gap={"2px"}
        mb={"24px"}
      >
        {[
          { text: "همه دوره ها", value: "All" },
          { text: "پیانو", value: "Piano" },
          { text: "کودکان", value: "Kids" },
          { text: "آهنگ ها", value: "Songs" },
          { text: "تئوری", value: "Theory" },
        ].map((item) => (
          <Button
            borderRadius={"0"}
            bgColor={
              category == item.value ? "transparent" : "rgba(52, 52, 52, 1)"
            }
            color={"white"}
            fontWeight={400}
            _hover={{ bgColor: "transparent" }}
            onClick={() => setCategory(item.value)}
          >
            {item.text}
          </Button>
        ))}
      </Flex>
      <Grid
        bgColor={"rgba(61, 61, 61, 1)"}
        gap={"24px"}
        gridTemplateColumns={{
          base: "1fr",
          sm: itemsToShow > 2 ? "repeat(3,1fr)" : `repeat(${itemsToShow},1fr)`,
        }}
        paddingY={"48px"}
        justifyItems={"center"}
        w={"100%"}
        minH={"400px"}
      >
        <Suspense isloading={packagesQuery.status != "success"}>
          {itemsToShow == 0 && <Text>دوره‌ای یافت نشد!</Text>}
          {packagesQuery.data
            .filter((predicate) => {
              if (category == "All") return true;
              return predicate.category == category;
            })
            .map((polished_package_noVideo) => (
              <GridItem>
                <Card imageSrc={polished_package_noVideo.covers[0]} w={200}>
                  <Flex
                    flexDir={"column"}
                    width={"100%"}
                    bgColor={"rgba(99, 99, 99, 1)"}
                  >
                    <Text
                      textAlign={"center"}
                      fontSize={"0.75em"}
                      padding={"12px"}
                    >
                      {polished_package_noVideo.name}
                    </Text>
                    <SimpleGrid
                      padding={"4px"}
                      minChildWidth="72px"
                      fontSize={"0.75em"}
                      gap={"6px"}
                    >
                      <GridItem>1</GridItem>
                      <GridItem>2</GridItem>
                      <GridItem>3</GridItem>
                      <GridItem>
                        <Flex gap={"4px"}>
                          <Text>قیمت:</Text>
                          <Text>{polished_package_noVideo.price}</Text>
                        </Flex>
                      </GridItem>
                    </SimpleGrid>
                    <LoadingButton
                      href={`packages/${polished_package_noVideo.slug}`}
                      w={"100%"}
                      borderRadius={"none"}
                      bgColor={"rgba(79, 79, 79, 1)"}
                      color={"white"}
                      fontWeight={300}
                      fontSize={"0.75em"}
                      _hover={{
                        bgColor: "rgba(120, 120, 120, 1)",
                      }}
                    >
                      خرید دوره
                    </LoadingButton>
                  </Flex>
                </Card>
              </GridItem>
            ))}
        </Suspense>
      </Grid>
    </>
  );
}
