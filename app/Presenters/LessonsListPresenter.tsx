import React from "react";
import { abstract_lesson, query } from "../types/frontTypes";
import { Box, Flex, Grid, GridItem, Heading, Text } from "@chakra-ui/react";
import Suspense from "../components/Suspense";
import { CardLoading } from "../components/LinkLoading";

export const LessonsListPresenter = ({
  query: lessonsQuery = { data: undefined, status: "loading" },
}: {
  query?: query<abstract_lesson[] | undefined>;
}) => {
  console.log(lessonsQuery);

  return (
    <Box padding={"40px"}>
      <Flex
        flexDir={"column"}
        alignItems={"center"}
        justifyContent={"center"}
        bgColor={"rgba(61, 61, 61, 1)"}
        padding={"24px"}
      >
        <Heading
          w={"100%"}
          fontSize={{ base: "1em", sm: "1.25em" }}
          fontWeight={400}
          textAlign={"end"}
        >
          سایر دروس دوره
        </Heading>
        <Grid
          gap={"24px"}
          gridTemplateColumns={{
            base: "1fr",
            sm:
              lessonsQuery.data && lessonsQuery.data.length > 2
                ? "repeat(3,1fr)"
                : `repeat(${
                    lessonsQuery.data != undefined
                      ? lessonsQuery.data.length
                      : 1
                  },1fr)`,
          }}
          paddingY={"24px"}
          justifyItems={"center"}
          w={"100%"}
          minH={"250px"}
        >
          <Suspense isloading={lessonsQuery.status != "success"}>
            {lessonsQuery.data != undefined &&
              lessonsQuery.data.length == 0 && (
                <Text dir="rtl">در حال حاضر درس دیگری این دوره ندارد!</Text>
              )}
            {lessonsQuery.data != undefined &&
              lessonsQuery.data.map((abstract_lesson) => (
                <GridItem>
                  <CardLoading
                    href={`/packages/${abstract_lesson.package}/${abstract_lesson.ordering}`}
                    imageSrc={abstract_lesson.cover}
                  >
                    <Flex
                      alignItems={"center"}
                      justifyContent={"center"}
                      w={"100%"}
                      padding={"12px"}
                      bgColor={"rgba(79, 79, 79, 1)"}
                    >
                      <Text textAlign={"center"} fontSize={"0.75em"}>
                        {abstract_lesson.title}
                      </Text>
                    </Flex>
                  </CardLoading>
                </GridItem>
              ))}
          </Suspense>
        </Grid>
      </Flex>
    </Box>
  );
};
