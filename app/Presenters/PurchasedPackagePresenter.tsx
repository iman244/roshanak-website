import Suspense from "@/app/components/Suspense";
import { polished_package_noVideo, query } from "@/app/types/frontTypes";
import React from "react";
import { FlexMain } from "../components/FlexMain";
import { InfoBox } from "../components/TextComponenets";
import { Flex, Heading, Text } from "@chakra-ui/react";
import { SimpleLesson } from "../components/SimpleLesson";

export default function PurchasedPackagePresenter({
  query: packageQuery = { data: undefined, status: "loading" },
}: {
  query?: query<polished_package_noVideo | undefined>;
}) {
  return (
    <FlexMain>
      <Flex w={"100%"} flexDir={"column"} gap={"24px"} alignItems={"center"}>
        <InfoBox heading={"توضیح در مورد کلیات دوره"}>
          <Text>asd</Text>
        </InfoBox>
        <InfoBox heading={"توضیح در مورد جزییات دوره"}>
          <Text>asd</Text>
        </InfoBox>
        <Flex
          w={"100%"}
          flexDir={"column"}
          gap={"24px"}
          padding={"24px"}
          bgColor={"rgba(217, 217, 217, 0.1)"}
        >
          <Heading fontWeight={"400"} fontSize={"1em"}>
            ویدیوهای دوره
          </Heading>
          <Suspense isloading={packageQuery.status == "loading"}>
            {packageQuery.data?.lessons.map(
              (simple_lesson, index) =>
                index == 0 && <SimpleLesson simple_lesson={simple_lesson} />
            )}
          </Suspense>
        </Flex>
      </Flex>
    </FlexMain>
  );
}
