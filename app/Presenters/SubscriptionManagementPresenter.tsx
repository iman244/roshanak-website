import { Flex, Heading, Text } from "@chakra-ui/react";
import React from "react";
import { GradientText } from "../components/TextComponenets";
import Link from "next/link";

export const SubscriptionManagementPresenter = () => {
  return (
    <Flex flexDir={"column"} alignItems={"center"} gap={"24px"}>
      <Flex
        w={"100%"}
        flexDir={"column"}
        gap={"24px"}
        padding={"24px"}
        border={"2px solid transparent"}
        borderRadius={"12px"}
        background={
          "linear-gradient(rgba(61,61,61,1), rgba(61,61,61,1)) padding-box,linear-gradient(90deg, #21C6E2 0%, #6B88EB 45.29%, #B549F6 99.95%) border-box;"
        }
      >
        <Heading fontWeight={"400"} fontSize={"1.2em"} textAlign={"center"}>
          مدیریت اشتراک
        </Heading>
        <Flex alignItems={"center"} gap={"8px"}>
          <Text>وضعیت اشتراک:</Text>
          <GradientText>فعال</GradientText>
        </Flex>
        <Flex alignItems={"center"} gap={"8px"}>
          <Text>نوع اشتراک:</Text>
          <GradientText form={false}>
            شش ماهه ( تاریخ انقضا ۱۴۰۲/۰۶/۱۴)
          </GradientText>
        </Flex>
      </Flex>
      <Flex w={"100%"} flexDir={"column"} gap={"24px"} padding={"24px"}>
        <Heading fontWeight={"400"} fontSize={"1.2em"} textAlign={"center"}>
          خرید و شارژ اشتراک
        </Heading>
        <Flex
          alignItems={"center"}
          w={"100%"}
          justifyContent={"space-between"}
          gap={"24px"}
          flexDir={{ base: "column", md: "row" }}
        >
          <Link href="#">
            <Flex
              padding={"8px 16px"}
              borderRadius={"12px"}
              border={"2px solid transparent"}
              background={
                "linear-gradient(rgba(61,61,61,1), rgba(61,61,61,1)) padding-box,linear-gradient(90deg, #21C6E2 0%, #6B88EB 45.29%, #B549F6 99.95%) border-box;"
              }
            >
              <Text
                fontWeight={"300"}
                fontSize={"0.75em"}
                _hover={{ textDecoration: "underline" }}
              >
                سه ماهه- 1000000 تومان
              </Text>
            </Flex>
          </Link>
          <Link href="#">
            <Flex
              padding={"8px 16px"}
              borderRadius={"12px"}
              border={"2px solid transparent"}
              background={
                "linear-gradient(rgba(61,61,61,1), rgba(61,61,61,1)) padding-box,linear-gradient(90deg, #21C6E2 0%, #6B88EB 45.29%, #B549F6 99.95%) border-box;"
              }
            >
              <Text
                fontWeight={"300"}
                fontSize={"0.75em"}
                _hover={{ textDecoration: "underline" }}
              >
                شش ماهه - 2000000 تومان
              </Text>
            </Flex>
          </Link>
          <Link href="#">
            <Flex
              padding={"8px 16px"}
              borderRadius={"12px"}
              border={"2px solid transparent"}
              background={
                "linear-gradient(rgba(61,61,61,1), rgba(61,61,61,1)) padding-box,linear-gradient(90deg, #21C6E2 0%, #6B88EB 45.29%, #B549F6 99.95%) border-box;"
              }
            >
              <Text
                fontWeight={"300"}
                fontSize={"0.75em"}
                _hover={{ textDecoration: "underline" }}
              >
                یک ساله - 3000000 تومان
              </Text>
            </Flex>
          </Link>
        </Flex>
      </Flex>
    </Flex>
  );
};
