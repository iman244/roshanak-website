import React from "react";
import { polished_package_noVideo, query } from "../types/frontTypes";
import { Button, Flex, Grid, GridItem, Heading, Text } from "@chakra-ui/react";
import { FullWidthHeading } from "../components/FullWidthHeading";
import Suspense from "../components/Suspense";
import { CardLoading, LinkLoading } from "../components/LinkLoading";

export const PackagesListSimplePresenter = ({
  query: packagesQuery = { data: [], status: "loading" },
}: {
  query?: query<polished_package_noVideo[]>;
}) => {
  return (
    <Flex flexDir={"column"} alignItems={"center"} justifyContent={"center"}>
      <FullWidthHeading>
        <Flex
          w={"100%"}
          maxW={"1000px"}
          alignItems={"center"}
          justifyContent={"space-between"}
          paddingX={"12px"}
        >
          <Heading fontSize={{ base: "1em", sm: "1.25em" }} fontWeight={400}>
            دوره های آموزش پیانو
          </Heading>
          <LinkLoading href={"packages"}>
            <Button
              variant={"link"}
              color={"white"}
              fontSize={{ base: "0.875em", sm: "1em" }}
              fontWeight={400}
            >
              تمامی دوره ها
            </Button>
          </LinkLoading>
        </Flex>
      </FullWidthHeading>
      <Grid
        gridTemplateColumns={{
          base: "1fr",
          sm:
            packagesQuery.data.length > 2
              ? "repeat(3,1fr)"
              : `repeat(${packagesQuery.data.length},1fr)`,
        }}
        paddingY={"48px"}
        justifyItems={"center"}
        w={"100%"}
      >
        <Suspense isloading={packagesQuery.status != "success"}>
          {packagesQuery.data.length == 0 && (
            <Text>در حال حاضر دوره‌ای در حال برگزاری نمی‌باشد</Text>
          )}
          {packagesQuery.data.map((polished_package) => (
            <GridItem>
              <CardLoading
                href={`packages/${polished_package.slug}`}
                imageSrc={polished_package.covers[0]}
              >
                <Flex
                  alignItems={"center"}
                  justifyContent={"center"}
                  w={"100%"}
                  padding={"12px"}
                >
                  <Text textAlign={"center"}>{polished_package.name}</Text>
                </Flex>
              </CardLoading>
            </GridItem>
          ))}
        </Suspense>
      </Grid>
    </Flex>
  );
};
