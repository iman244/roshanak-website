import Suspense from "@/app/components/Suspense";
import { polished_package_noVideo, query } from "@/app/types/frontTypes";
import React from "react";
import { FlexMain } from "../components/FlexMain";
import { Flex, Heading, Text } from "@chakra-ui/react";
import { InfoBox } from "../components/TextComponenets";
import { SimpleLesson } from "../components/SimpleLesson";
import { ColorfulButton } from "../components/ButtonComponents";

export default function PackagePresenter({
  query: packageQuery = { data: undefined, status: "loading" },
}: {
  query?: query<polished_package_noVideo | undefined>;
}) {
  console.log("jep");
  return (
    <FlexMain>
      <Flex
        flexDir={{ base: "column", md: "row" }}
        gap={"24px"}
        w={"100%"}
        maxW={"1000px"}
      >
        <Flex
          padding={"24px"}
          bgColor={"rgba(217, 217, 217, 0.1)"}
          maxW={{ base: "100%", md: "250px" }}
          height={"fit-content"}
        >
          <Suspense isloading={packageQuery.status != "success"}>
            <BuyInfoBox packageQuery={packageQuery} />
          </Suspense>
        </Flex>
        <ContentPackage packageQuery={packageQuery} />
      </Flex>
    </FlexMain>
  );
}

const ContentPackage = ({
  packageQuery,
}: {
  packageQuery: query<polished_package_noVideo | undefined>;
}) => {
  return (
    <Flex w={"100%"} flexDir={"column"} gap={"24px"} alignItems={"center"}>
      <InfoBox heading={"توضیح در مورد کلیات دوره"}>
        <Text>asd</Text>
      </InfoBox>
      <InfoBox heading={"توضیح در مورد جزییات دوره"}>
        <Text>asd</Text>
      </InfoBox>
      <InfoBox heading={"مخاطبین دوره"}>
        <Text>asd</Text>
      </InfoBox>
      <InfoBox heading={"خروجی دوره"}>
        <Text>asd</Text>
      </InfoBox>
      <Flex
        w={"100%"}
        flexDir={"column"}
        gap={"24px"}
        padding={"24px"}
        bgColor={"rgba(217, 217, 217, 0.1)"}
      >
        <Heading fontWeight={"400"} fontSize={"1em"}>
          ویدیوهای دوره
        </Heading>
        <Suspense isloading={packageQuery.status == "loading"}>
          {packageQuery.data?.lessons.map((simple_lesson, index) => (
            <SimpleLesson
              href={`./${packageQuery.data?.slug}/${index + 1}`}
              simple_lesson={simple_lesson}
            />
          ))}
        </Suspense>
      </Flex>
    </Flex>
  );
};

const BuyInfoBox = ({
  packageQuery,
}: {
  packageQuery: query<polished_package_noVideo | undefined>;
}) => {
  return (
    <Flex
      flexDir={"column"}
      alignItems={"center"}
      w={"100%"}
      gap={"24px"}
      // height={"fit-content"}
    >
      <Flex
        alignItems={"center"}
        gap={"8px"}
        w={"100%"}
        // height={"fit-content"}
      >
        <Text>{packageQuery.data?.price}</Text>
        <Text>تومان</Text>
      </Flex>
      <ColorfulButton w={{ base: "100%", md: "fit-content" }}>
        خرید دوره
      </ColorfulButton>
      <Flex flexDir={"column"} gap={"8px"} w={"100%"}>
        <Heading fontWeight={"400"} fontSize={"1em"}>
          این دوره شامل:
        </Heading>
        <Flex alignItems={"center"} gap={"8px"} w={"100%"}>
          <Text fontSize={"0.75em"}>{packageQuery.data?.lessons.length}</Text>
          <Text fontSize={"0.75em"}>قسمت</Text>
        </Flex>
        <Flex alignItems={"center"} gap={"8px"} w={"100%"}>
          <Text fontSize={"0.75em"}>سطح:</Text>
          <Text fontSize={"0.75em"}>
            {packageQuery.data?.level == 1
              ? "مقدماتی"
              : packageQuery.data?.level == 2
              ? "متوسط"
              : "پیشرفته"}
          </Text>
        </Flex>
        <Text fontSize={"0.75em"}>گواهی پایان دوره</Text>
      </Flex>
    </Flex>
  );
};
