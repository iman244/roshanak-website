import { query } from "@/app/types/frontTypes";
import React, { useEffect, useState } from "react";
import Form, {
  EmailInput,
  LastNameInput,
  NameInput,
  PhoneNumberInput,
  ProfileImageInput,
  SubmitButton,
} from "../components/Form";
import { AspectRatio, Box, Flex, Grid, GridItem } from "@chakra-ui/react";
import { useFormContext } from "react-hook-form";

function EditProfilePresenter({ query: profileQuery }: { query?: query<any> }) {
  const [isloading, setIsLoading] = useState(false);

  return (
    <Form
      url="/accounts-must-check-the-true-url"
      onSubmitF={(data) => {
        setIsLoading(true);
        return data;
      }}
      onSettledF={() => setIsLoading(false)}
      case200={(data) => {
        return data;
      }}
    >
      <Flex alignItems={"center"} flexDir={"column"}>
        <Grid
          padding={"48px"}
          bgColor={"rgba(61, 61, 61, 1)"}
          maxW={"800px"}
          gridTemplateColumns={{ base: "1fr", md: "repeat(2,1fr)" }}
          gridTemplateRows={{ base: "repeat(6,auto)", md: "repeat(4,1fr)" }}
          gridAutoFlow={"column"}
          columnGap={"48px"}
          rowGap={"24px"}
          height={"fit-content"}
        >
          <GridItem>
            <EmailInput />
          </GridItem>
          <GridItem>
            <NameInput />
          </GridItem>
          <GridItem>
            <LastNameInput />
          </GridItem>
          <GridItem>
            <PhoneNumberInput />
          </GridItem>
          <GridItem rowSpan={{ base: 1, md: 3 }}>
            <Flex
              flexDir={"column"}
              width={"100%"}
              alignItems={"center"}
              gap={"12px"}
            >
              <AspectRatio w={{ base: "60%", md: "100%" }} ratio={1}>
                <ImagePreview />
              </AspectRatio>
              <ProfileImageInput />
            </Flex>
          </GridItem>
          <GridItem>
            <SubmitButton isloading={isloading} />
          </GridItem>
        </Grid>
      </Flex>
    </Form>
  );
}

export default EditProfilePresenter;

const ImagePreview = () => {
  const { getValues } = useFormContext();

  const [image, setImage] = useState(null);
  useEffect(() => {
    setImage(getValues("image"));
  }, [getValues("image")]);
  return (
    <Box
      bgColor={"gray"}
      backgroundImage={image ? URL.createObjectURL(image) : ""}
      backgroundPosition={"center"}
      backgroundSize={"contain"}
      backgroundRepeat={"no-repeat"}
    ></Box>
  );
};
