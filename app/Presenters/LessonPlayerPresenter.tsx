import Suspense from "@/app/components/Suspense";
import { extracted_lesson_for_player, query } from "@/app/types/frontTypes";
import React, { useState, useReducer, useEffect } from "react";
import { FlexMain } from "../components/FlexMain";
import {
  Box,
  Flex,
  Heading,
  Icon,
  Text,
  chakra,
  shouldForwardProp,
} from "@chakra-ui/react";
import ReactPlayer from "react-player";
import { AiOutlineFilePdf } from "react-icons/ai";
import { isValidMotionProp, motion } from "framer-motion";
import { useRouter } from "next/navigation";

function changeVideo(state: number, action: any) {
  switch (action.type) {
    case "change-video":
      return action.videoOrdering;
    case "next-video":
      if (state >= action.videosLength) {
        action.nextLesson();
        return action.videosLength;
      } else {
        return state + 1;
      }
    default:
      return 1;
  }
}

export default function LessonPresenter({
  query: lessonQuery = { data: undefined, status: "loading" },
}: {
  query?: query<extracted_lesson_for_player | undefined>;
}) {
  // const [videoOrdering, setVideoOrdering] = useState(1);
  const [videoOrdering, dispatch] = useReducer(changeVideo, 1);
  const router = useRouter();

  useEffect(() => {
    console.log("useEffect", videoOrdering);
  }, [videoOrdering]);
  console.log(lessonQuery);
  return (
    <FlexMain>
      <Flex width={"100%"} gap={"24px"}>
        <Flex
          flexDir={"column"}
          w={{ base: "100%", sm: "200px" }}
          gap={"16px"}

          // maxW={{ base: "100%", sm: "200px" }}
        >
          <Flex
            padding={"12px 8px"}
            bgColor={"rgba(217, 217, 217, 0.1)"}
            minH={"60px"}
          >
            <Suspense isloading={lessonQuery.status != "success"}>
              <Flex alignItems={"center"} justifyContent={"center"} gap={"8px"}>
                <Text
                  as={"a"}
                  href={lessonQuery.data?.pdf}
                  target="_blank"
                  fontSize={"0.7em"}
                  _hover={{
                    bgGradient:
                      "linear-gradient(90deg, #6B88EB 0%, #B549F6 100%);",
                    bgClip: "text",
                  }}
                >
                  دانلود فایل نت {lessonQuery.data?.title}
                </Text>
                <Icon as={AiOutlineFilePdf} />
              </Flex>
            </Suspense>
          </Flex>
          <Flex bgColor={"rgba(217, 217, 217, 0.1)"} paddingY={"8px"}>
            <Suspense
              isloading={lessonQuery.status != "success"}
              fallback={<VideoListLoading />}
            >
              <Flex flexDir={"column"} alignItems={"center"} w={"100%"}>
                <Heading fontSize={"1em"} fontWeight={400} mb={"16px"}>
                  لیست ویدیوهای درس
                </Heading>
                {lessonQuery.data?.videos.map((videoInfo) => {
                  return (
                    <Flex
                      gap={"8px"}
                      alignItems={"center"}
                      padding={"8px"}
                      _hover={{
                        bgColor: "rgba(50, 50, 50, 0.5)",
                        cursor: "pointer",
                      }}
                      onClick={() => {
                        dispatch({
                          type: "change-video",
                          videoOrdering: videoInfo.ordering,
                        });
                      }}
                    >
                      <Text fontSize={"0.75em"} flex={1}>
                        {videoInfo.title}
                      </Text>
                      <Box
                        flex={2}
                        borderRadius={"8px"}
                        backgroundPosition={"center"}
                        backgroundSize={"cover"}
                        backgroundImage={lessonQuery.data?.cover}
                        w={"100px"}
                        height={"60px"}
                      />
                    </Flex>
                  );
                })}
              </Flex>
            </Suspense>
          </Flex>
        </Flex>
        <Flex
          flexDir={"column"}
          w={"100%"}
          height={"100%"}
          bgColor={"rgba(217, 217, 217, 0.1)"}
          minH={"calc((100vw - 260px) / 16 * 9)"}
        >
          <Suspense
            isloading={lessonQuery.status != "success" || !lessonQuery.data}
          >
            <VideoPlayer
              nextVideo={() => {
                dispatch({
                  type: "next-video",
                  videosLength: lessonQuery.data?.videos.length,
                  nextLesson: () => {
                    lessonQuery.data?.nextLessonURL &&
                      router.push(lessonQuery.data?.nextLessonURL);
                  },
                });
              }}
              video={
                lessonQuery.data!?.videos.filter((videoInfo) => {
                  return videoInfo.ordering == videoOrdering;
                })[0].video
              }
            />
            {lessonQuery.data && (
              <Text padding={"16px"}>
                {lessonQuery.data?.title} -{" ویدیو "}
                {
                  lessonQuery.data!?.videos.filter((videoInfo) => {
                    return videoInfo.ordering == videoOrdering;
                  })[0].title
                }
              </Text>
            )}
          </Suspense>
        </Flex>
      </Flex>
    </FlexMain>
  );
}

const VideoPlayer = ({
  video: videoString,
  nextVideo,
}: {
  video: string;
  nextVideo: any;
}) => {
  const [playing, setPlaying] = useState(false);

  return (
    <ReactPlayer
      url={videoString}
      controls
      width={"100%"}
      height={"unset"}
      onStart={() => {
        setPlaying(true);
      }}
      onPause={() => setPlaying(false)}
      onProgress={({ playedSeconds }) => {
        if (!playing) setPlaying(true);
        // if (
        //   Math.floor(playedSeconds) %
        //     (Math.ceil(trackTime) / (staveNumber / staveToShow)) ==
        //   0
        // ) {
        //   setAnimationTrigger(Math.floor(playedSeconds));
        // }
      }}
      onEnded={nextVideo}
    />
  );
};

const VideoListLoading = () => {
  return (
    <ChakraDiv
      display={"flex"}
      flexDir={"column"}
      alignItems={"center"}
      w={"100%"}
      as={motion.div}
      animate={{
        opacity: [0.8, 0.6, 0.8],
      }}
      transition={{
        ease: "linear",
        duration: 1,
        repeat: Infinity,
        repeatType: "loop",
      }}
    >
      {[...Array(3)].map(() => (
        <Flex
          gap={"8px"}
          alignItems={"center"}
          padding={"8px"}
          _hover={{
            bgColor: "rgba(50, 50, 50, 0.5)",
            cursor: "pointer",
          }}
          w={"100%"}
        >
          <Flex
            flexDir={"column"}
            w={"100%"}
            flex={1}
            alignItems={"flex-end"}
            gap={"4px"}
          >
            <Box
              fontSize={"0.75em"}
              w={"100%"}
              bg={"rgba(50,50,50 , 0.7)"}
              height={"12px"}
              borderRadius={"6px"}
            ></Box>
            <Box
              fontSize={"0.75em"}
              w={"60%"}
              bg={"rgba(160,160,160 , 0.3)"}
              height={"12px"}
              borderRadius={"6px"}
            ></Box>
          </Flex>
          <Box
            flex={2}
            borderRadius={"8px"}
            backgroundPosition={"center"}
            backgroundSize={"cover"}
            // backgroundImage={lessonQuery.data?.cover}
            bgColor={"rgba(160,160,160 , 0.3)"}
            w={"100px"}
            height={"60px"}
          />
        </Flex>
      ))}
    </ChakraDiv>
  );
};

export const ChakraDiv = chakra(motion.div, {
  /**
   * Allow motion props and non-Chakra props to be forwarded.
   */
  shouldForwardProp: (prop) => {
    return isValidMotionProp(prop) || shouldForwardProp(prop);
  },
});
