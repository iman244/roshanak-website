import Suspense from "@/app/components/Suspense";
import { polished_package_withVideo, query } from "@/app/types/frontTypes";
import React from "react";
import { FlexMain } from "../components/FlexMain";
import { Flex, Grid, GridItem, Heading, Text } from "@chakra-ui/react";
import Link from "next/link";
import { Card } from "../components/Card";

export default function PurchasedPackagesPresenter({
  query: ppQuery = { data: [], status: "loading" },
}: {
  query?: query<polished_package_withVideo[] | undefined>;
}) {
  console.log("ppQuery", ppQuery);

  return (
    <Flex
      flexDir={"column"}
      width={"100%"}
      bgColor={"rgba(61, 61, 61, 1)"}
      alignItems={"center"}
      padding={"24px"}
    >
      <Heading fontWeight={"400"} fontSize={"1.2em"}>
        دوره‌های خریداری‌شده
      </Heading>
      <Grid
        gap={"24px"}
        gridTemplateColumns={{
          base: "1fr",
          sm:
            ppQuery.data!?.length > 3
              ? "repeat(4,1fr)"
              : `repeat(${ppQuery.data?.length},1fr)`,
        }}
        paddingY={"48px"}
        justifyItems={"center"}
        w={"100%"}
      >
        <Suspense isloading={ppQuery.status != "success"}>
          {ppQuery.data!.length == 0 && (
            <Text>در حال حاضر دوره‌ای در حال برگزاری نمی‌باشد</Text>
          )}
          {ppQuery.data!.map((polished_package_withVideo) => (
            <GridItem>
              <Link href={`packages/${polished_package_withVideo.slug}`}>
                <Card imageSrc={polished_package_withVideo.covers[0]}>
                  <Flex
                    alignItems={"center"}
                    justifyContent={"center"}
                    w={"100%"}
                    padding={"12px"}
                    bgColor={"rgba(100, 100, 100, 1)"}
                    _hover={{ bgColor: "rgba(120, 120, 120, 1)" }}
                  >
                    <Text textAlign={"center"}>
                      {polished_package_withVideo.name}
                    </Text>
                  </Flex>
                </Card>
              </Link>
            </GridItem>
          ))}
        </Suspense>
      </Grid>
    </Flex>
  );
}
