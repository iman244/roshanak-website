"use client";

import React from "react";
// import { QueryClientProvider, QueryClient } from "react-query";
import { ReactQueryStreamedHydration } from "@tanstack/react-query-next-experimental";
import { ChakraProvider } from "@chakra-ui/react";
import { CookiesProvider } from "react-cookie";
import { QueryClient, QueryClientProvider } from "react-query";

const queryClient = new QueryClient();
function Providers({ children }: React.PropsWithChildren) {
  // const [client] = React.useState(new QueryClient());

  return (
    <ChakraProvider>
      <CookiesProvider>
        <QueryClientProvider client={queryClient}>
          <ReactQueryStreamedHydration queryClient={queryClient}>
            {children}
          </ReactQueryStreamedHydration>
        </QueryClientProvider>
      </CookiesProvider>
    </ChakraProvider>
  );
}

export default Providers;
