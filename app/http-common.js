import axios from "axios";
export const backendIP = "http://188.121.101.112:8081";

export default axios.create({
  baseURL: `${backendIP}/api/V0.0.0`,
  withCredentials: true,
  headers: {
    "Content-type": "application/json",
  },
});
