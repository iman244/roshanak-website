"use client";

import { FlexMain } from "@/app/components/FlexMain";
import {
  EmailInput,
  FormLayout,
  PasswordInput,
  SubmitButton,
} from "@/app/components/Form";
import { GradientText } from "@/app/components/TextComponenets";
import {
  LoginDTO,
  LoginErrorData,
  LoginSuccessData,
} from "@/app/types/backendTypes";
import { Flex, Icon, Text } from "@chakra-ui/react";
import { useRouter } from "next/navigation";
import React, { useState } from "react";
import { AiFillUnlock, AiOutlineUser } from "react-icons/ai";
import Link from "next/link";

export default function SignIn(props: any) {
  const router = useRouter();

  const [isLoading, setIsLoading] = useState(false);
  const [loginResponse, setLoginResponse] = useState<{ message: string }>({
    message: "",
  });
  return (
    <FlexMain>
      <FormLayout
        heading="ورود"
        url="/accounts/login/email/"
        onSubmitF={(data: { email: string; password: string }): LoginDTO => {
          setIsLoading(true);
          return data;
        }}
        onSettledF={() => {
          setIsLoading(false);
        }}
        case200={(data: LoginSuccessData) => {
          setLoginResponse({ message: "با موفقیت وارد شدید" });
          const { access_token, refresh_token } = data;
          localStorage.setItem("access_token", access_token);
          localStorage.setItem("refresh_token", refresh_token);
          router.push("/");
        }}
        case401={(data: LoginErrorData) => {
          setLoginResponse({ message: "ایمیل یا رمزعبور اشتباه است!" });
        }}
      >
        <Flex
          flexDir={"column"}
          alignItems={"center"}
          justifyContent={"center"}
          w={"100%"}
        >
          <GradientText>{loginResponse.message}</GradientText>
          <Flex
            alignItems={{ base: "center", md: "flex-start" }}
            justifyContent={"space-between"}
            w={"100%"}
            flexDir={{ base: "column", md: "row" }}
          >
            <Flex width={{ base: "100%", md: "40%" }}>
              <EmailInput />
            </Flex>
            <Flex
              width={{ base: "100%", md: "40%" }}
              flexDir={"column"}
              gap={"8px"}
            >
              <PasswordInput />
              <ForgotPasswordLink />
            </Flex>
          </Flex>
        </Flex>
        <Flex
          alignSelf={"flex-end"}
          alignItems={"center"}
          gap={"24px"}
          mt={"32px"}
        >
          <SignUpLink />
          <SubmitButton isLoading={isLoading}>ورود</SubmitButton>
        </Flex>
      </FormLayout>
    </FlexMain>
  );
}

const ForgotPasswordLink = () => {
  return (
    <Link href={"/authenticate/forgot-password"}>
      <Flex
        alignItems={"center"}
        gap={"4px"}
        _hover={{ textDecoration: "underline" }}
      >
        <Icon as={AiFillUnlock} boxSize={3} />
        <Text fontSize={"0.625em"}>رمز عبورم را فراموش کرده ام</Text>
      </Flex>
    </Link>
  );
};

const SignUpLink = () => {
  return (
    <Link href={"/authenticate/sign-up"}>
      <Flex
        alignItems={"center"}
        gap={"4px"}
        _hover={{ textDecoration: "underline" }}
      >
        <Icon as={AiOutlineUser} boxSize={4} />
        <Text fontSize={"0.875em"}>ثبت نام</Text>
      </Flex>
    </Link>
  );
};
