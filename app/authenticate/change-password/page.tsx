"use client";

import { FlexMain } from "@/app/components/FlexMain";
import {
  FormLayout,
  PasswordInput,
  SubmitButton,
  VerficationCodeInput,
} from "@/app/components/Form";
import { GradientText } from "@/app/components/TextComponenets";
import { Grid, GridItem } from "@chakra-ui/react";
import React, { useState } from "react";

export default function ChangePassword(props: any) {
  console.log(props);
  const [isLoading, setIsLoading] = useState(false);
  const [cpResponse, setCPResponse] = useState<{ message: string }>({
    message: "",
  });
  return (
    <FlexMain>
      <FormLayout
        heading="تغییر رمز عبور"
        url={"/accounts/change-password"}
        onSubmitF={(data: { email: string }) => {
          setIsLoading(true);
          return data;
        }}
        onSettledF={() => {
          setIsLoading(false);
        }}
        case200={(data: any) => {
          console.log("200", data);
        }}
        case401={(data: any) => {
          console.log("401", data);
        }}
      >
        <GradientText>{cpResponse.message}</GradientText>
        <Grid
          gridTemplateColumns={{ base: "1fr", md: "repeat(2,1fr)" }}
          w={"100%"}
          h={"100%"}
          columnGap={"48px"}
          rowGap={"24px"}
        >
          <GridItem>
            <PasswordInput />
          </GridItem>
          <GridItem mt={"32px"}>
            <SubmitButton isLoading={isLoading}>تغییر رمز عبور</SubmitButton>
          </GridItem>
        </Grid>
      </FormLayout>
    </FlexMain>
  );
}
