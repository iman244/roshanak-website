"use client";

import { FlexMain } from "@/app/components/FlexMain";
import {
  FormLayout,
  SubmitButton,
  VerficationCodeInput,
} from "@/app/components/Form";
import { GradientText } from "@/app/components/TextComponenets";
import { Flex, Grid, GridItem } from "@chakra-ui/react";
import React, { useState } from "react";

export default function RetriveVerficationCode(props: any) {
  console.log(props);
  const [isLoading, setIsLoading] = useState(false);
  const [vcResponse, setVCResponse] = useState<{ message: string }>({
    message: "کد تغییر رمزعبور به ایمیل شما ارسال شده است.",
  });
  return (
    <FlexMain>
      <FormLayout
        heading="احراز کد تأیید"
        url={"/accounts/verfication-code-url-is-not-correct"}
        onSubmitF={(data: { email: string }) => {
          setIsLoading(true);
          return data;
        }}
        onSettledF={() => {
          setIsLoading(false);
        }}
        case200={(data: any) => {
          console.log("200", data);
        }}
        case401={(data: any) => {
          console.log("401", data);
        }}
      >
        <GradientText>{vcResponse.message}</GradientText>
        <Grid
          gridTemplateColumns={{ base: "1fr", md: "repeat(2,1fr)" }}
          w={"100%"}
          h={"100%"}
          columnGap={"48px"}
          rowGap={"24px"}
        >
          <GridItem>
            <VerficationCodeInput />
          </GridItem>
          <GridItem mt={"32px"}>
            <SubmitButton isLoading={isLoading}>ارسال کد</SubmitButton>
          </GridItem>
        </Grid>
      </FormLayout>
    </FlexMain>
  );
}
