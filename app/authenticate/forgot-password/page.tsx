"use client";

import { FlexMain } from "@/app/components/FlexMain";
import { EmailInput, FormLayout, SubmitButton } from "@/app/components/Form";
import { GradientText } from "@/app/components/TextComponenets";
import { Flex, Grid, GridItem } from "@chakra-ui/react";
import React, { useState } from "react";

export default function ForgotPassword(props: any) {
  console.log(props);
  const [isLoading, setIsLoading] = useState(false);
  const [fpResponse, setFpResponse] = useState<{ message: string }>({
    message: "",
  });
  return (
    <FlexMain>
      <FormLayout
        heading="بازیابی رمز عبور"
        url={"/accounts/forgot-password"}
        onSubmitF={(data: { email: string }) => {
          setIsLoading(true);
          return data;
        }}
        onSettledF={() => {
          setIsLoading(false);
        }}
        case200={(data: any) => {
          console.log("200", data);
        }}
        case401={(data: any) => {
          console.log("401", data);
        }}
      >
        <GradientText>{fpResponse.message}</GradientText>
        <Grid
          gridTemplateColumns={{ base: "1fr", md: "repeat(2,1fr)" }}
          w={"100%"}
          h={"100%"}
          columnGap={"48px"}
          rowGap={"24px"}
        >
          <GridItem>
            <EmailInput />
          </GridItem>
          <GridItem mt={"32px"}>
            <SubmitButton isLoading={isLoading}>ارسال کد</SubmitButton>
          </GridItem>
        </Grid>
      </FormLayout>
    </FlexMain>
  );
}
