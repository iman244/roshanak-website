"use client";

import { ColorfulButton } from "@/app/components/ButtonComponents";
import { FlexMain } from "@/app/components/FlexMain";
import {
  EmailInput,
  FormLayout,
  LastNameInput,
  NameInput,
  PasswordInput,
  PhoneNumberInput,
  SignUpLaw,
  SubmitButton,
} from "@/app/components/Form";
import { GradientText } from "@/app/components/TextComponenets";
import { Flex, Grid, GridItem, Spinner } from "@chakra-ui/react";
import { useRouter } from "next/navigation";
import React, { useState } from "react";

export default function SignUp(props: any) {
  const router = useRouter();

  const [isLoading, setIsLoading] = useState(false);
  const [signUpResponse, setSignUpResponse] = useState<{ message: string }>({
    message: "",
  });

  console.log("props", props);
  return (
    <FlexMain>
      <FormLayout
        heading="ثبت نام"
        url="/accounts/signup/"
        onSubmitF={(data: any) => {
          setIsLoading(true);
          var { phone_number } = data;
          phone_number = "+98" + phone_number;
          return {
            ...data,
            phone_number,
          };
        }}
        onSettledF={() => {
          setIsLoading(false);
        }}
        case200={(data) => {
          setSignUpResponse({ message: "با موفقیت ثبت نام شدید" });
          const { access_token, refresh_token } = data;
          localStorage.setItem("access_token", access_token);
          localStorage.setItem("refresh_token", refresh_token);
          // router.push("/");
        }}
        case401={(data) => {
          console.log(data);
          setSignUpResponse(data);
        }}
      >
        <GradientText>{signUpResponse.message}</GradientText>
        <Grid
          gridTemplateColumns={{ base: "1fr", md: "repeat(2,1fr)" }}
          // gridTemplateColumns={"1fr"}
          w={"100%"}
          h={"100%"}
          columnGap={"48px"}
          rowGap={"24px"}
        >
          <GridItem>
            <NameInput />
          </GridItem>
          <GridItem>
            <LastNameInput />
          </GridItem>
          <GridItem>
            <EmailInput />
          </GridItem>
          <GridItem>
            <PhoneNumberInput />
          </GridItem>
          <GridItem>
            <PasswordInput />
          </GridItem>
          <GridItem colSpan={{ base: 1, md: 2 }}>
            <SignUpLaw />
          </GridItem>
          <GridItem colSpan={{ base: 1, md: 2 }}>
            <SubmitButton isLoading={isLoading}>ثبت نام</SubmitButton>
          </GridItem>
        </Grid>
      </FormLayout>
    </FlexMain>
  );
}
