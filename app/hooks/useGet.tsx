import axios, { AxiosError } from "axios";
import apiClient from "../http-common";
import { useQuery } from "react-query";
import {
  GETerrorThenFunctions,
  GETsuccessThenFunctions,
  GETunknownThenFunctions,
} from "../types/restTypes";

export default function useGet(
  url: string,
  querykeys: any[],
  options: {
    onSettledF?: GETunknownThenFunctions;
    onSubmitF?: GETsuccessThenFunctions;
    case200?: GETsuccessThenFunctions;
    case201?: GETsuccessThenFunctions;
    case401?: GETsuccessThenFunctions;
    case404?: GETsuccessThenFunctions;
    caseError?: GETerrorThenFunctions;
  } = {}
) {
  var Q;

  var { onSettledF, onSubmitF, case200, case201, case401, case404, caseError } =
    options;

  Q = useQuery(
    [...querykeys],
    () => {
      var access_token = localStorage.getItem("access_token");
      return apiClient
        .get(url, {
          headers: {
            Authorization: `${access_token}`,
          },
        })
        .then((res) => res.data);
    },
    {
      onSettled(data, error) {
        onSettledF && onSettledF(data, error);
      },
      onSuccess(d): any {
        const { status } = d;
        const data = d.data;
        switch (status) {
          case 200:
            case200 && case200(data);
            break;
          case 201:
            case201 && case201(data);
            break;
          case 401:
            case401 && case401(data);
            break;
          default:
            break;
        }
      },
      onError(error: any) {
        if (axios.isAxiosError(error)) {
          const { response } = error;
          const d = response!.data;
          const { status, data } = d;
          switch (status) {
            case 401:
              case401 && case401(data);
              break;
            case 404:
              case404 && case404(data);
              break;
            default:
              break;
          }
        }

        caseError && caseError(error);
      },
    }
  );
  return Q;
}
