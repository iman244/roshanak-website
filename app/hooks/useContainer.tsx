import React, { useEffect, useState } from "react";
import { query } from "../types/frontTypes";
import useGet from "./useGet";
import { useRouter } from "next/navigation";

interface options<D, E> {
  keys: any[];
  defaultValue?: D;
  case200?: (data: any) => D;
  case401?: (data: any) => E;
  case404?: (data: any) => void;
}

type container<D, E> = (url: string, options: options<D, E>) => query<D>;

export function useContainer<D, E = any>(
  url: string,
  options: options<D, E>
): query<D | E | undefined> {
  const router = useRouter();
  const [dataQuery, setDataQuery] = useState<query<D | E | undefined>>({
    data: options.defaultValue,
    status: "loading",
  });
  const [data, setData] = useState<D | E | undefined>(options.defaultValue);
  const GET = useGet(url, options.keys, {
    case200: (data) => {
      if (options.case200) {
        const pData = options.case200(data);
        setData(pData);
      } else {
        setData(data);
      }
    },
    case401: (data) => {
      if (options.case401) {
        const pData = options.case401(data);
        setData(pData);
      } else {
        router.push("/authenticate/sign-in");
      }
    },
    case404: options.case404 && options.case404,
  });

  useEffect(() => {
    setDataQuery({ data, status: GET.status });
  }, [data]);

  return dataQuery;
}

export function ChildrenWithProps(children: any, query: any) {
  return React.Children.map(children, (child) => {
    if (React.isValidElement(child)) {
      return React.cloneElement<any>(child, { query: query });
    }
    return child;
  });
}
