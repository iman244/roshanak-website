"use client";
import { useContext } from "react";

import { LanguageContext } from "./contexts/LanguageContext";
import {
  AspectRatio,
  Flex,
  Grid,
  GridItem,
  Heading,
  Text,
  useBreakpointValue,
  Image as ChakraImage,
  Box,
} from "@chakra-ui/react";
import { BorderCard, Card } from "./components/Card";
import { FullWidthHeading } from "./components/FullWidthHeading";
import Image from "next/image";
import { InstallLink } from "./components/InstallLink";
import { GrayBox } from "./components/GrayBox";
import {
  NextArrow,
  PreviousArrow,
  SlideContainer,
  slideItemsStyle,
} from "./components/Slide";
import { Banner } from "./components/BoxPiano";
import { PackagesContainer } from "./Containers/PackagesContainer";
import { PackagesListSimplePresenter } from "./Presenters/PackagesListSimplePresenter";

export default function Home() {
  const { language } = useContext(LanguageContext);
  return (
    <main style={{ direction: language == "fa" ? "rtl" : "ltr", padding: "0" }}>
      <HeadingPage />
      <InfoSection />
      <PackagesContainer>
        <PackagesListSimplePresenter />
      </PackagesContainer>
      <Flex
        w={"100%"}
        alignItems={"center"}
        justifyContent={"center"}
        padding={"48px"}
      >
        <SlideBanner />
      </Flex>
      <VIPPackageIntro />
      <InstallLinks />
      <Subscription />
      <StudentsComments />
    </main>
  );
}

const HeadingPage = () => {
  return (
    <Flex paddingTop={"32px"} dir={"rtl"}>
      <Box style={{ flex: 1 }}>
        <Image
          src={"/roshanakPhoto.png"}
          alt="roshanak"
          width={300}
          height={100}
        />
      </Box>

      <Flex
        flexDir={"column"}
        gap={1}
        flex={1}
        alignItems={"center"}
        justifyContent={"center"}
      >
        <ChakraImage
          alt="logo"
          src="/icon-512x512.png"
          width={{ base: "80px", sm: "150px" }}
        />
        <Flex flexDir={"column"} fontSize={{ base: "0.875em", sm: "1.25em" }}>
          <Text textAlign={"center"} textTransform={"uppercase"}>
            roshanak
          </Text>
          <Text textAlign={"center"} textTransform={"uppercase"}>
            piano
          </Text>
        </Flex>
      </Flex>
    </Flex>
  );
};

const InfoSection = () => {
  return (
    <Flex
      bgColor={"rgba(61, 61, 61, 1)"}
      flexDir={"column"}
      alignItems={"center"}
      justifyContent={"center"}
      padding={"64px"}
      mb={"48px"}
    >
      <Text textAlign={"center"}>
        توضیح جدید درمورد کلیت دوره ها (مثلا اینکه تمامی دوره ها انلاین هستن
        بعلاوه تصویر مناسب بنر)
      </Text>
      <Text textAlign={"center"}>
        ادرس و لینک اینستاگرام و شماره تلفن و واتسپ و ...
      </Text>
    </Flex>
  );
};

const SlideBanner = () => {
  const slideWidth = useBreakpointValue({
    base: "80vw",
    sm: "60vw",
    md: "40vw",
  });
  const ArrowUseWidth = useBreakpointValue({
    base: "95vw",
    sm: "75vw",
    md: "50vw",
  });
  const settings = {
    dots: true,
    infinite: true,
    centerMode: false,
    autoplay: true,
    autoplaySpeed: 3000,
    speed: 1000,
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow: (
      <NextArrow
        arrowIconProps={slideItemsStyle.arrowIconProps}
        arrowProps={slideItemsStyle.arrowProps}
        slideElementSize={ArrowUseWidth}
      />
    ),
    prevArrow: (
      <PreviousArrow
        arrowIconProps={slideItemsStyle.arrowIconProps}
        arrowProps={slideItemsStyle.arrowProps}
        slideElementSize={ArrowUseWidth}
      />
    ),
  };

  return (
    <SlideContainer settings={settings} w={"100vw"}>
      <Banner backgroundImage={"banner1.png"} w={slideWidth} />
      <Banner backgroundImage={"banner1.png"} w={slideWidth} />
    </SlideContainer>
  );
};

const VIPPackageIntro = () => {
  return (
    <Flex
      flexDir={"column"}
      justifyContent={"center"}
      alignItems={"center"}
      dir="rtl"
    >
      <FullWidthHeading>
        <Heading fontSize={"1em"} fontWeight={400}>
          ویژگی‌های پکیج VIP
        </Heading>
      </FullWidthHeading>
      <Grid
        gridTemplateColumns={{
          base: "1fr",
          sm: "repeat(2,1fr)",
          md: "repeat(3,1fr)",
        }}
        paddingY={"24px"}
        justifyItems={"center"}
        w={"100%"}
        maxW={"1000px"}
        gap={"24px"}
      >
        <GridItem>
          <GrayBox>
            <Text fontSize={{ base: "0.75em", sm: "1em" }} fontWeight={400}>
              توضیح درباره پشتیبانی و ...
            </Text>
          </GrayBox>
        </GridItem>
        <GridItem>
          <GrayBox>
            <Text fontSize={{ base: "0.75em", sm: "1em" }} fontWeight={400}>
              تعداد جلسات و ساعت ها و ...
            </Text>
          </GrayBox>
        </GridItem>
        <GridItem colSpan={{ base: 1, sm: 2, md: 1 }}>
          <GrayBox>
            <Text fontSize={{ base: "0.75em", sm: "1em" }} fontWeight={400}>
              توضیحات دیگر
            </Text>
          </GrayBox>
        </GridItem>
      </Grid>
    </Flex>
  );
};

const InstallLinks = () => {
  return (
    <Flex
      alignItems={"center"}
      justifyContent={"center"}
      gap={"32px"}
      mb={"24px"}
      flexDir={{ base: "column", sm: "row" }}
    >
      <Image src={"/phone.png"} alt={"app preview"} width={200} height={400} />
      <Flex
        flexDir={"column"}
        alignItems={"center"}
        justifyContent={"center"}
        gap={"16px"}
      >
        {contactUsList.map((item) => (
          <InstallLink src={item.src} href={item.href} text={item.text} />
        ))}
      </Flex>
    </Flex>
  );
};

const Subscription = () => {
  return (
    <Flex
      flexDir={"column"}
      alignItems={"center"}
      justifyContent={"center"}
      mb={"24px"}
    >
      <FullWidthHeading>
        <Heading fontSize={"1em"} fontWeight={400}>
          اشتراک ویژه
        </Heading>
      </FullWidthHeading>
      <Grid
        gridTemplateColumns={{
          base: "1fr",
          sm: "repeat(2,1fr)",
          md: "repeat(4,1fr)",
        }}
        paddingY={"24px"}
        justifyItems={"center"}
        w={"100%"}
        maxW={"1000px"}
        gap={"24px"}
      >
        {[
          {
            src: "/music.png",
            text: "توضیح توضیح توضیح توضیح توضیح توضیح توضیح توضیح",
          },
          {
            src: "/library.png",
            text: "توضیح توضیح توضیح توضیح توضیح توضیح توضیح توضیح",
          },
          {
            src: "/note.png",
            text: "توضیح توضیح توضیح توضیح توضیح توضیح توضیح توضیح",
          },
          {
            src: "/rythm.png",
            text: "توضیح توضیح توضیح توضیح توضیح توضیح توضیح توضیح",
          },
        ].map((item) => (
          <GridItem>
            <Card imageSrc={item.src}>
              <AspectRatio ratio={1} w={"160px"}>
                <Flex
                  alignItems={"flex-start !important"}
                  justifyContent={"flex-start !important"}
                >
                  <Text>{item.text}</Text>
                </Flex>
              </AspectRatio>
            </Card>
          </GridItem>
        ))}
      </Grid>
    </Flex>
  );
};

const StudentsComments = () => {
  return (
    <Flex
      flexDir={"column"}
      alignItems={"center"}
      justifyContent={"center"}
      mb={"48px"}
      bgColor={"rgba(61, 61, 61, 1)"}
    >
      <FullWidthHeading>
        <Heading fontSize={"1.25em"} fontWeight={400} padding={"12px"}>
          نظرات هرجویان دوره های برگزارشده
        </Heading>
      </FullWidthHeading>
      <Grid
        gridTemplateColumns={{
          base: "1fr",
          md: "repeat(3,1fr)",
        }}
        paddingY={"48px"}
        justifyItems={"center"}
        w={"100%"}
        gap={"48px"}
      >
        {[
          {
            src: "/music.png",
            studentName: "نام هنرجو",
            packageName: "نام دوره نام دوره",
            studentComment:
              "نظر هنرجو درباره دوره نظر هنرجو درباره دوره نظر هنرجو درباره دوره نظر هنرجو درباره دوره نظر هنرجو درباره دوره نظر هنرجو درباره دوره نظر هنرجو درباره دوره نظر هنرجو درباره دوره نظر هنرجو درباره دوره نظر هنرجو درباره دوره نظر هنرجو درباره دوره ",
          },
          {
            src: "/library.png",
            studentName: "نام هنرجو",
            packageName: "نام دوره نام دوره",
            studentComment:
              "نظر هنرجو درباره دوره نظر هنرجو درباره دوره نظر هنرجو درباره دوره نظر هنرجو درباره دوره نظر هنرجو درباره دوره نظر هنرجو درباره دوره نظر هنرجو درباره دوره نظر هنرجو درباره دوره نظر هنرجو درباره دوره نظر هنرجو درباره دوره نظر هنرجو درباره دوره ",
          },
          {
            src: "/note.png",
            studentName: "نام هنرجو",
            packageName: "نام دوره نام دوره",
            studentComment:
              "نظر هنرجو درباره دوره نظر هنرجو درباره دوره نظر هنرجو درباره دوره نظر هنرجو درباره دوره نظر هنرجو درباره دوره نظر هنرجو درباره دوره نظر هنرجو درباره دوره نظر هنرجو درباره دوره نظر هنرجو درباره دوره نظر هنرجو درباره دوره نظر هنرجو درباره دوره ",
          },
        ].map((item) => (
          <GridItem>
            <BorderCard imageSrc={item.src}>
              <Flex
                alignItems={"center"}
                flexDir={"column"}
                padding={"16px"}
                paddingTop={"47px"}
                gap={"6px"}
              >
                <Text
                  fontSize={"0.75em"}
                  fontWeight={"300"}
                  textAlign={"center"}
                >
                  {item.studentName}
                </Text>
                <Text
                  fontSize={"0.75em"}
                  fontWeight={"300"}
                  textAlign={"center"}
                  color={"rgba(187, 187, 187, 1)"}
                >
                  {item.packageName}
                </Text>
                <Text
                  fontSize={"0.75em"}
                  fontWeight={"300"}
                  textAlign={"center"}
                >
                  {item.studentComment}
                </Text>
              </Flex>
            </BorderCard>
          </GridItem>
        ))}
      </Grid>
    </Flex>
  );
};

export const contactUsList = [
  { src: "/androidApkLogo.png", text: "بازار", href: "" },
  { src: "/cafeBazzarLogo.png", text: "سیب اپ", href: "" },
  { src: "/googlePlayLogo.png", text: "گوگل پلی", href: "" },
  { src: "/sibappLogo.png", text: "اندروید APK", href: "" },
];
