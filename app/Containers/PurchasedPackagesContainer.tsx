"use client";

import React from "react";
import { polishPurchasedPackages } from "@/app/Functions/polishBackendData";
import { ChildrenWithProps, useContainer } from "../hooks/useContainer";
import { polished_package_withVideo } from "../types/frontTypes";

export default function PurchasedPackagesContainer({ children }: any) {
  const ppQuery = useContainer<polished_package_withVideo[]>(
    "/packages/owned/",
    {
      defaultValue: [],
      keys: ["purchased_packages"],
      case200(data) {
        return polishPurchasedPackages(data);
      },
    }
  );
  return <>{ChildrenWithProps(children, ppQuery)}</>;
}
