import React from "react";
import { ChildrenWithProps, useContainer } from "../hooks/useContainer";
import { extractLessonPlayerData } from "../Functions/polishBackendData";
import { useRouter } from "next/navigation";
import { extracted_lesson_for_player } from "../types/frontTypes";

export const LessonContainer = ({
  children,
  slug,
  lesson_ordering,
}: {
  children: any;
  slug: string;
  lesson_ordering: string;
}) => {
  const router = useRouter();
  const lessonQuery = useContainer<extracted_lesson_for_player | undefined>(
    `/packages/owned/${slug}`,
    {
      keys: [`/lesson_player/${slug}`],
      case200: (data) => {
        try {
          return extractLessonPlayerData(data, Number(lesson_ordering));
        } catch (error) {
          router.push(`/packages/${slug}`);
        }
      },
    }
  );

  return <>{ChildrenWithProps(children, lessonQuery)}</>;
};
