import React from "react";
import { ChildrenWithProps, useContainer } from "../hooks/useContainer";
import { abstract_lesson } from "../types/frontTypes";
import { extractAbstractLesson } from "../Functions/polishBackendData";
import { useRouter } from "next/navigation";

export const LessonsContainer = ({
  children,
  slug,
  exception = false,
  lesson_ordering,
}: {
  children: any;
  slug: string;
  exception?: boolean;
  lesson_ordering: number;
}) => {
  const router = useRouter();
  const lessonsQuery = useContainer<abstract_lesson[] | undefined>(
    `/packages/owned/${slug}`,
    {
      defaultValue: undefined,
      keys: [`/lessons/${slug}`, lesson_ordering],
      case200: (data) => {
        try {
          let abstractLessons = extractAbstractLesson(data);
          if (exception) {
            return extractAbstractLesson(data).filter(
              (abstract_lesson) => abstract_lesson.ordering != lesson_ordering
            );
          }
          return abstractLessons;
        } catch (error) {
          console.log(error);
          // router.push(`/packages/${slug}`);
        }
      },
    }
  );

  return <>{ChildrenWithProps(children, lessonsQuery)}</>;
};
