"use client";

import React from "react";
import { polishPackageWithVideo } from "@/app/Functions/polishBackendData";
import { ChildrenWithProps, useContainer } from "../hooks/useContainer";
import { polished_package_withVideo } from "../types/frontTypes";

export default function PurchasedPackagesContainer({
  children,
  slug,
}: {
  children: any;
  slug: string;
}) {
  const ppQuery = useContainer<polished_package_withVideo>(
    `/packages/owned/${slug}`,
    {
      defaultValue: undefined,
      keys: [`purchased_package/${slug}`],
      case200(data) {
        return polishPackageWithVideo(data);
      },
    }
  );
  return <>{ChildrenWithProps(children, ppQuery)}</>;
}
