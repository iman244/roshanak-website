import React from "react";
import { ChildrenWithProps, useContainer } from "../hooks/useContainer";
import { polishPackageNoVideo } from "../Functions/polishBackendData";
import { backend_package_noVideo } from "../types/backendTypes";
import { useRouter } from "next/navigation";
import { polished_package_noVideo } from "../types/frontTypes";

export const PackageContainer = ({
  children,
  slug,
}: {
  children: any;
  slug: string;
}) => {
  const router = useRouter();
  const packageSlugQuery = useContainer<polished_package_noVideo>(
    `packages/${slug}`,
    {
      keys: [slug],
      defaultValue: undefined,
      case200: (data: backend_package_noVideo) => {
        return polishPackageNoVideo(data);
      },

      case404(data) {
        router.push("/not-found");
      },
    }
  );

  return <>{ChildrenWithProps(children, packageSlugQuery)}</>;
};
