"use client";

import React from "react";
import { backend_packages } from "../types/backendTypes";
import { polishPackages } from "../Functions/polishBackendData";
import { ChildrenWithProps, useContainer } from "../hooks/useContainer";

export const PackagesContainer = ({ children }: any) => {
  const packagesQuery = useContainer("packages/", {
    keys: ["packages"],
    defaultValue: [],
    case200: (data: backend_packages) => {
      return polishPackages(data);
    },
  });

  return <>{ChildrenWithProps(children, packagesQuery)}</>;
};
