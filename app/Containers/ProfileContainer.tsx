"use client";

import { backend401, backend_profile } from "@/app/types/backendTypes";
import React, { useEffect } from "react";
import { ChildrenWithProps, useContainer } from "../hooks/useContainer";
import { usePathname, useRouter } from "next/navigation";

export default function ProfileContainer({ children }: any) {
  const pathname = usePathname();
  const router = useRouter();
  const profileQuery = useContainer<backend_profile, backend401>(
    "/accounts/profile/",
    {
      keys: ["profile", pathname],
      case401(data) {
        return data;
      },
    }
  );

  useEffect(() => {
    console.log(router);
  }, [router]);

  return <>{ChildrenWithProps(children, profileQuery)}</>;
}
