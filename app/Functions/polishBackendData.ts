import { backendIP } from "../http-common";
import {
  backend_package_noVideo,
  backend_package_withVideo,
  backend_packages,
  backend_purchased_packages,
} from "../types/backendTypes";
import {
  abstract_lesson,
  extracted_lesson_for_player,
  full_lesson,
  polished_package_noVideo,
  polished_package_withVideo,
  simple_lesson,
} from "../types/frontTypes";

type polishPackage_noVideo = (
  backend_package: backend_package_noVideo
) => polished_package_noVideo;

type polishPackage_withVideo = (
  backend_package: backend_package_withVideo
) => polished_package_withVideo;

type polishPackages = (
  backend_packages: backend_packages
) => polished_package_noVideo[];

type polishPackagesOwned = (
  backend_packages_owned: backend_purchased_packages
) => polished_package_withVideo[];

type extractLessonPlayerData = (
  backend_package: backend_package_withVideo,
  lesson_ordering: number
) => extracted_lesson_for_player;

type extractAbstractLesson = (
  backend_package: backend_package_withVideo
) => abstract_lesson[];

export const polishPackages: polishPackages = (
  backend_packages: backend_packages
) => {
  return backend_packages.packages_list[0].map(
    (backend_package_noVideo, index, array) =>
      polishPackageNoVideo(backend_package_noVideo)
  );
};

export const polishPurchasedPackages: polishPackagesOwned = (
  backend_packages_owned
) => {
  return backend_packages_owned.owned_packages.map(
    (backend_package_withVideo) =>
      polishPackageWithVideo(backend_package_withVideo)
  );
};

export const polishPackageNoVideo: polishPackage_noVideo = (
  backend_package_noVideo
) => {
  var { lessons } = backend_package_noVideo;

  let covers = backend_package_noVideo.package_covers.map(
    (coverObject) => `${backendIP}${coverObject.cover.file}`
  );

  let polishedLessons: simple_lesson[] = lessons.map((lesson) => {
    return {
      ...lesson,
      voice: `${backendIP}${lesson?.voice?.file}`,
    };
  });

  return {
    ...backend_package_noVideo,
    covers,
    lessons: polishedLessons,
  };
};

export const polishPackageWithVideo: polishPackage_withVideo = (
  backend_package_withVideo
) => {
  const { lessons } = backend_package_withVideo;

  let covers = backend_package_withVideo.package_covers.map((coverObject) =>
    ConcatBackendIP(coverObject.cover.file)
  );

  let polishedLessons: full_lesson[] = lessons.map((backend_full_lesson) => {
    return {
      ...backend_full_lesson,
      midi: ConcatBackendIP(backend_full_lesson.midi.file),
      videos: backend_full_lesson.videos.map((backendVideoInfo, index) => {
        return {
          title: backendVideoInfo.title,
          ordering: index + 1,
          video: ConcatBackendIP(backendVideoInfo.video.file),
        };
      }),
      voice: ConcatBackendIP(backend_full_lesson?.voice?.file),
      video: ConcatBackendIP(backend_full_lesson?.video?.uploaded_video.file),
      pdf: ConcatBackendIP(backend_full_lesson?.pdf?.file),
      cover: ConcatBackendIP(backend_full_lesson?.cover?.file),
    };
  });

  return {
    ...backend_package_withVideo,
    lessons: polishedLessons,
    covers,
  };
};

function ConcatBackendIP(document: string) {
  return `${backendIP}${document}`;
}

export const extractLessonPlayerData: extractLessonPlayerData = (
  backend_package,
  lesson_ordering
) => {
  const Package = polishPackageWithVideo(backend_package);
  const lesson = Package.lessons.filter((full_lesson) => {
    return full_lesson.ordering == lesson_ordering;
  });
  var next_lesson = Package.lessons.filter((full_lesson) => {
    return full_lesson.ordering == lesson_ordering + 1;
  });

  if (next_lesson.length == 0) {
    next_lesson = lesson;
  }

  if (lesson.length == 0) {
    console.log(
      `there is no such a lesson, this package have ${backend_package.lessons.length} lesson, you order lesson number ${lesson_ordering}`
    );
    // throw `there is no such a lesson, this package have ${backend_package.lessons.length} lesson, you order lesson number ${lesson_ordering}`;
  }

  const specificLesson = lesson[0];

  return {
    title: specificLesson.title,
    cover: specificLesson.cover,
    pdf: specificLesson.pdf,
    midi: specificLesson.midi,
    videos: specificLesson.videos,
    nextLessonURL: `/packages/${Package.slug}/${next_lesson[0].ordering}`,
    package: Package.slug,
  };
};

export const extractAbstractLesson: extractAbstractLesson = (
  backend_package
) => {
  const Package = polishPackageWithVideo(backend_package);
  return Package.lessons.map((full_lesson) => {
    return {
      title: full_lesson.title,
      ordering: full_lesson.ordering,
      cover: full_lesson.cover,
      package: Package.slug,
    };
  });
};
